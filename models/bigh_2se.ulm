{ Two-sex matrix model for bighorn sheep with polygynous mating function,
{ demographic stochasticity and environmental stochasticity.
{ The computed probabilities of extinction match those reported by Berger (1990).
{ Including inbreeding depression further improves the match.
{ See related file bigh_02.ulm.

{ Legendre S. Influence of age structure and mating system
{ on population viability. In Evolutionary Conservation Biology,
{ R Ferriere, U Dieckmann & D Couvet eds. Cambridge University Press. pp. 41-58.
{
{ Berger J. 1990. Persistence of different sized populations:
{ an empirical assessment of rapid extinction in bighorn sheep.
{ Conservation Biology 4:91-98.
{
{ Example of commands:
{	change nm7 4		( propagule size = 20 = 4 adult males + 16 adult females
{	change nf7 16		( according to breeding sex ratio
{	montecarlo 70 1000	( give extinction probabilities along time
{	change nf7 8		( propagule size = 40
{	change nf7 32
{	montecarlo 70 1000	( give extinction probabilities along time


defmod bigh_2se(14)
rel : rf1,rf2,rf3,rf4,rf5,rf6,rf7,rm1,rm2,rm3,rm4,rm5,rm6,rm7

{ --- Relations for females  ---

defrel rf1
nf1 = binomf(nf1a,sf0)

defrel rf2
nf2 = binomf(nf1,sf1)

defrel rf3
nf3 = binomf(nf2,sf2)

defrel rf4
nf4 = binomf(nf3,sf3)

defrel rf5
nf5 = binomf(nf4,sf4)

defrel rf6
nf6 = binomf(nf5,sf5)

defrel rf7
nf7 = binomf(nf6,sf6) + binomf(nf7,vf)

{ --- Relations for females  ---

defrel rm1
nm1 =  binomf(nm1a,s0)

defrel rm2
nm2 = binomf(nm1,sm1)

defrel rm3
nm3 = binomf(nm2,sm2)

defrel rm4
nm4 = binomf(nm3,sm3)

defrel rm5
nm5 = binomf(nm4,sm4)

defrel rm6
nm6 = binomf(nm5,sm5)

defrel rm7
nm7 = binomf(nm6,sm6) + binomf(nm7,vm)

{ initial population sizes:
defvar nm1 = 0

defvar nm2 = 0

defvar nm3 = 0

defvar nm4 = 0

defvar nm5 = 0

defvar nm6 = 0

defvar nm7 = 4

defvar nmtot = nm1+nm2+nm3+nm4+nm5+nm6+nm7

defvar nf1 = 0

defvar nf2 = 0

defvar nf3 = 0

defvar nf4 = 0

defvar nf5 = 0

defvar nf6 = 0

defvar nf7 = 16

defvar nftot = nf1+nf2+nf3+nf4+nf5+nf6+nf7

defvar ntot = nmtot+nftot

{ standard deviation for environmental noise
defvar dd = 0.2

{ male survival rates with environmental stochasticity:
defvar s0 = 0.57

defvar sm0 = beta1f(s0,dd)

defvar sm1 = beta1f(0.86,dd)

defvar sm2 = beta1f(0.86,dd)

defvar sm3 = beta1f(0.78,dd)

defvar sm4 = beta1f(0.78,dd)

defvar sm5 = beta1f(0.78,dd)

defvar sm6 = beta1f(0.78,dd)

defvar vm  = beta1f(0.63,dd)

{ female survival rates with environmental stochasticity:
defvar sf0 = beta1f(s0,dd)

defvar sf1 = beta1f(0.83,dd)

defvar sf2 = beta1f(0.94,dd)

defvar sf3 = beta1f(0.94,dd)

defvar sf4 = beta1f(0.94,dd)

defvar sf5 = beta1f(0.94,dd)

defvar sf6 = beta1f(0.94,dd)

defvar vf  = beta1f(0.85,dd)

{ primary female sex ratio
defvar sigma = 0.5

{ fecundities with environmental stochasticity:
defvar f = 0.7

defvar f3 = beta1f(f,dd)

defvar f4 = beta1f(f,dd)

defvar f5 = beta1f(f,dd)

defvar f6 = beta1f(f,dd)

defvar f7 = beta1f(f,dd)

{ number of reproducing females
defvar nf = nf3+nf4+nf5+nf6+nf7

{ number of reproducing males
defvar nm = nm5+nm6+nm7

{ breeding sex ratio
defvar rho = if(nm+nf,nf/(nm+nf),0)

{ average harem size
defvar h = 4

{ number of polygynous matings
defvar mat = trunc(min(h*nm,nf))

{ matings are dispatched among age classes:
defvar mat3 = if(nf,trunc(mat*nf3/nf),0)

defvar mat4 = if(nf,trunc(mat*nf4/nf),0)

defvar mat5 = if(nf,trunc(mat*nf5/nf),0)

defvar mat6 = if(nf,trunc(mat*nf6/nf),0)

defvar mat7 = if(nf,trunc(mat*nf7/nf),0)

{ female production
defvar pf = poissonf(mat3,f3) + poissonf(mat4,f4) + poissonf(mat5,f5) + poissonf(mat6,f6) + poissonf(mat7,f7)

{ number of female offspring
defvar nf1a = binomf(pf,sigma)

{ number of male offspring
defvar nm1a = pf - nf1a

{ effective population size
defvar ne = 4*nm*rho
