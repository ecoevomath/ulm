{ Model for the size-classified life cycle of the palm tree Astrocaryum mexicanum.

{ Pinero D, M Martinez-Ramos & J Sarukhan. 1984.
{ A population model of Astrocaryum mexicanum and a sensitivity analysis of its finite rate of increase.
{ Journal of Ecology 72:977-999.

{ Cochran ME & S Ellner. 1992.
{ Simple methods for calculating age-specific life history parameters from stage-structured models.
{ Ecological Monographs 62:345-364.

{ Bienvenu F & S Legendre. 2015.
{ A new approach to the generation time in matrix population models.
{ The American Naturalist 185:834-843.

{ Note that the character '#' is used as a suffix to specify reproductive transitions.

{ Type p (for property) in the interp window to compute the generation time T.

defmod astrocaryum(10)
mat : a
vec : v

defvec v(10)
n1,n2,n3,n4,n5,n6,n7,n8,n9,n10

defmat a(10)
0,       0,       0,       0,       0,       1.4792#, 8.1560#, 9.9513#, 14.259#, 23.594#
0.037349,0.83093, 0,       0,       0,       0,       0,       0,       0,       0
0,       0.015881,0.89666, 0,       0,       0,       0,       0,       0,       0
0,       0,       0.048969,0.95944, 0,       0,       0,       0,       0,       0
0,       0,       0,       0.029778,0.90496, 0,       0,       0,       0,       0
0,       0,       0,       0,       0.082074,0.91348, 0,       0,       0,       0
0,       0,       0,       0,       0,       0.086520,0.90553, 0,       0,       0
0,       0,       0,       0,       0,       0,       0.094467,0.87733, 0,       0
0,       0,       0,       0,       0,       0,       0,       0.088200,0.88642, 0
0,       0,       0,       0,       0,       0,       0,       0,       0.11358, 0.995

defvar n1 n2 n3 n4 n5 n6 n7 n8 n9 n10 = 1

defvar n = n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10
