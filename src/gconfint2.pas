{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gconfint2;

{$MODE Delphi}

{ @@@@@@  form to display confidence interval of growth rate     @@@@@@ }
{ @@@@@@  knowing standard deviations of demographic parameters  @@@@@@ }

interface

uses SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Grids, Buttons,
     jglobvar, jmath;

const maxdempar = 20;

type
  tform_confint2 = class(TForm)
    left_panel: TPanel; 
    right_panel: TPanel; 
    top_panel: TPanel; 
    stringgrid_dempar: TStringGrid;
    label_dempar: TLabel;
    label_mat: TLabel;
    label_lambda: TLabel;
    Edit_mat: TEdit;
    label_sigma_lambda: TLabel;
    label_lambda_plus: TLabel;
    label_lambda_moins: TLabel;
    edit_z_alpha: TEdit;
    label_z_alpha: TLabel;
    label_confidence: TLabel;
    button_ok: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure init_confint2;
    procedure erreur(s : string);
    procedure Edit_matReturnPressed(Sender: TObject);
    procedure eigenval(x : integer; var err : boolean);
    procedure eigenvec(x : integer; var err : boolean);
    procedure confint2(m : integer);
    procedure sigma_lambda1(m : integer; var err : boolean);
    function  sens_x(m,x : integer; var err : boolean) : extended;
    procedure stringgrid_demparSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure stringgrid_demparSetEditText(Sender: TObject; ACol,
      ARow: Integer; const Value: WideString);
    procedure edit_z_alphaReturnPressed(Sender: TObject);
    procedure button_okClick(Sender: TObject);
  private
    n : integer;        { size of matrix }
    x_mat : integer;    { pointer to current matrix }
    lambda : cvec_type; { complex eigenvalues }
    v,w : rvec_type;    { left, right eigenvectors associated with dominant eigenvalue }
    lambda1 : extended; { lambda1 = dominant eigenvalue }
    mat_sens : rmat_type;    { matrix of sensitivities }
    sigma_lambda : extended; { standard deviation of lambda1 }
    z_alpha : extended;  { lambda1 +/- z_alpha*sigma_lambda }
    alpha : extended;    { % confidence interval }
    nb_dempar : integer; { number of variables/demographic parameters }
    dempar,dempar1 : array[0..maxdempar] of integer; { pointers to these variables }
    dempar_sigma,dempar_sigma1 : array[0..maxdempar] of extended; { standard deviation of these variables }
  public
  end;

var form_confint2: tform_confint2;

implementation

uses jutil,jsymb,jsyntax,jeval;

{$R *.lfm}

procedure tform_confint2.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Confidence Interval 2 - ' + s);
end;

procedure tform_confint2.FormCreate(Sender: TObject);
begin
  x_mat := 0;
  z_alpha := 1.0;
end;

procedure tform_confint2.eigenval(x : integer;var err : boolean);
begin
  err := true;
  with mat[x] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then
        begin
          erreur('Dominant eigenvalue <= 0');
          exit;
        end;
    end;
  err := false;
end;

procedure tform_confint2.eigenvec(x : integer; var err : boolean);
var i : integer;
    vv,ww : cvec_type;
begin
  err := true;
  with mat[x] do
    begin
      matvecpropdroite(size,val,lambda[1],ww);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w);
      matvecpropgauche(size,val,lambda[1],vv);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do v[i] := vv[i].re;
      vecnormalise1(size,v);
    end;
  err := false;
end;

function  tform_confint2.sens_x(m,x : integer; var err : boolean) : extended;
{ sensitivity of dominant eigenvalue to changes in variable x }
var i,j,y,ty : integer;
    som,a : extended;
begin
  err := true;
  with mat[m] do
    begin
      som := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            deriv(exp[i,j],exp_type[i,j],x,y,ty); { compute derivative }
            if err_eval then
              begin
                err_eval := false;
                exit;
              end;
            a := eval(y,ty); { evaluate expression of derivative }
            som := som + mat_sens[i,j]*a;
          end;
      sens_x := som;
    end;
  err := false;
end;

procedure tform_confint2.sigma_lambda1(m : integer;var err : boolean);
{ compute standard deviation of dominant eigenvalue using sensitivies }
var i,j,k,x : integer;
    h,a,sx : extended;
begin
  err := true;
  with mat[m] do
    begin
      h := vecpscal(size,v,w);
      if ( h = 0.0 ) then exit;
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/h; { matrix of sensitivities }
      h := 0.0;
      for k := 1 to nb_dempar do
        begin
          x  := dempar[k]; { variable corresponding to demographic parameter }
          sx := sens_x(m,x,err); { sensitivity }
          a  := dempar_sigma[k]; { standard deviation }
          h  := h + sqr(sx*a);
        end;
      if ( h > 0.0 ) then
        sigma_lambda := sqrt(h)
      else
        sigma_lambda := 0.0;
    end;
  err := false;
end;

procedure tform_confint2.confint2(m : integer);
var err : boolean;
begin
  n := mat[m].size;
  edit_mat.Text := s_ecri_mat(m);
  eigenval(m,err);
  if err then exit;
  eigenvec(m,err);
  if err then exit;
  label_lambda.Caption := 'lambda = ' + Format('%1.6g',[lambda1]);
  sigma_lambda1(m,err);
  if err then exit;
  label_sigma_lambda.caption := 'Sigma_lambda = ' + Format('%10.6g',[sigma_lambda]);
  label_lambda_plus.caption  := 'Lambda + z*sigma = ' + Format('%10.6g',[lambda1 + z_alpha*sigma_lambda]);
  label_lambda_moins.caption := 'Lambda - z*sigma = ' + Format('%10.6g',[lambda1 - z_alpha*sigma_lambda]);
  alpha := 2.0*phinormal(z_alpha) - 1.0; { use cumulative distribution of the normal distribution }
  label_confidence.caption := '---> with ' + Format('%1.2g',[alpha*100.0]) + '% confidence';
end;

procedure tform_confint2.FormActivate(Sender: TObject);
var i,k : integer;
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      edit_mat.Text := '';
      exit;
    end;
  n := mat[x_mat].size;
  with stringgrid_dempar do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := 21;
      RowCount  := 4;
      for i := 1 to ColCount-1 do Cells[i,0] := IntToStr(i);
      Cells[0,1] := 'Name';
      Cells[0,2] := 'Value';
      Cells[0,3] := 'Sigma';
      Constraints.MinHeight := 6 * RowHeights[1];
      for k := 1 to nb_dempar do
        begin
          Cells[k,1] := s_ecri_var(dempar[k]);
          Cells[k,2] := s_ecri_val(variable[dempar[k]].val);
          Cells[k,3] := s_ecri_val(dempar_sigma[k]);
        end;
      for k := nb_dempar+1 to maxdempar do
        begin
          Cells[k,1] := '';
          Cells[k,2] := '';
          Cells[k,3] := '';
        end;
    end;
  confint2(x_mat); { display confidence interval }
  edit_z_alpha.Text := FloatToStr(z_alpha);
  AutoSize := False;
end;

procedure Tform_confint2.init_confint2;
var k : integer;
begin
  init_form(form_confint2);
  x_mat := 0;
  z_alpha := 1.0;
  nb_dempar := 0;
  for k := 1 to maxdempar do dempar_sigma[k] := 0.1; { default value }
  for k := 1 to modele_nb do
    if ( modele[k].xmat <> 0 ) then
      begin
        x_mat := modele[k].xmat;
        n := mat[x_mat].size;
        exit;
      end;
end;

procedure Tform_confint2.Edit_matReturnPressed(Sender: TObject);
{ name of matrix }
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Matrix: unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

procedure tform_confint2.stringgrid_demparSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  canselect := ( acol > 0 ) and ( arow = 1 ) or ( arow = 3 );
end;

procedure tform_confint2.stringgrid_demparSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: WideString);
begin
  stringgrid_dempar.AutoSizeColumn(ACol);
  stringgrid_dempar.ColWidths[Acol] := stringgrid_dempar.ColWidths[Acol] + 15;
end;

procedure tform_confint2.edit_z_alphaReturnPressed(Sender: TObject);
var a : extended;
    valid : boolean;
begin
  valid := TryStrToFloat(edit_z_alpha.Text,a);
  if valid then
    z_alpha := a
  else
    erreur('Invalid entry: parameter z');
  FormActivate(nil);
end;

procedure tform_confint2.button_okClick(Sender: TObject);
{ read demographic parameters names and their standard deviations from stringgrid }
var k,nb,x,tx : integer;
    s : string;
    valid : boolean;
    a : extended;
begin
  nb := 0;
  for k := 1 to maxdempar do
    begin
      s := stringgrid_dempar.Cells[k,1];
      if ( s <> '' )  then
        begin
          trouve_obj(s,x,tx);
          if ( tx = type_variable ) then
            begin
              nb := nb + 1;
              dempar1[nb] := x;
              if ( stringgrid_dempar.Cells[k,3] = '' ) then
                stringgrid_dempar.Cells[k,3] := FloatToStr(dempar_sigma[nb]);
              valid := TryStrToFloat(stringgrid_dempar.Cells[k,3],a);
              if valid then
                dempar_sigma1[nb] := a
              else
                begin
                  erreur('Invalid entry: sigma ' + s);
                  stringgrid_dempar.Cells[k,3] := FloatToStr(dempar_sigma[nb]);
                end;
            end
          else
            begin
              erreur('unknown variable name');
              exit;
            end;
        end;
    end;
  nb_dempar := nb;
  for k := 1 to nb_dempar do
    begin
      x := dempar1[k];
      dempar[k] := x;
      stringgrid_dempar.Cells[k,1] := s_ecri_var(x);
      dempar_sigma[k] := dempar_sigma1[k];
      stringgrid_dempar.Cells[k,2] := s_ecri_val(variable[x].val);
      stringgrid_dempar.Cells[k,3] := s_ecri_val(dempar_sigma[k]);
    end;
  for k := nb+1 to maxdempar do with stringgrid_dempar do
    begin
      Cells[k,1] := '';
      Cells[k,2] := '';
      Cells[k,3] := '';
    end;
  edit_mat.Text := s_ecri_mat(x_mat);
  confint2(x_mat);
end;

end.
