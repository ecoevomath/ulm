{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit jmatrix;

{$MODE Delphi}

{  @@@@@@   determine matrix type   @@@@@@  }

interface

uses jglobvar;

function  mattimedep(x : integer) : boolean;
function  matvecdep(x,y : integer) : boolean;
function  matrandom(x : integer): boolean;
function  matleslie(n : integer;a : rmat_type ) : boolean;
function  matleslie2(n : integer;a : rmat_type ) : boolean;
function  matclassetaille(n : integer;a : rmat_type ) : boolean;
function  matmultisite(n : integer;a : rmat_type;var p : integer) : boolean;
function  test_matmultisite(n,p : integer;a : rmat_type) : boolean;

implementation

uses SysUtils,jeval;

function  matleslie(n : integer;a : rmat_type ) : boolean;
{ check if a is a nxn Leslie matrix }
var i,j : integer;
    b : boolean;
begin
  b := true;
  for j := 1 to n do b := b and ( a[1,j] >= 0.0 );
  for i := 2 to n do b := b and ( a[i,i-1] >= 0.0 ) and ( a[i,i-1] <= 1.0 );
  for i := 2 to n do
    for j := 1 to n do
      if ( j <> i-1 ) then b := b and ( a[i,j] = 0.0 );
  matleslie := b;
end;

function  matleslie2(n : integer;a : rmat_type ) : boolean;
{ check if a is a nxn extended Leslie matrix ( 0 < a[n,n] < 1 ) }
var i,j : integer;
    b : boolean;
begin
  b := true;
  for j := 1 to n do b := b and ( a[1,j] >= 0.0 );
  for i := 2 to n do b := b and ( a[i,i-1] > 0.0 ) and ( a[i,i-1] <= 1.0 );
  for i := 2 to n-1 do
    for j := i to n do
      b := b and ( a[i,j] = 0.0 );
  for j := 1 to n-2 do
    for i := j+2 to n do
      b := b and ( a[i,j] = 0.0 );
  b := b and ( a[n,n] > 0.0  ) and ( a[n,n] < 1.0 );
  matleslie2 := b;
end;

function  matclassetaille(n : integer;a : rmat_type ) : boolean;
{ check if a is a nxn size-classified matrix }
var i,j : integer;
    b : boolean;
begin
  b := true;
  for j := 1 to n do b := b and ( a[1,j] >= 0.0 );
  for i := 2 to n do b := b and ( a[i,i] > 0.0 ) and ( a[i,i] < 1.0 );
  for i := 2 to n do b := b and ( a[i,i-1] > 0.0 ) and ( a[i,i-1] <= 1.0 );
  for i := 2 to n-1 do
    for j := i+1 to n do
      b := b and ( a[i,j] = 0.0 );
  for i := 3 to n do
    for j := 1 to i-2 do
      b := b and ( a[i,j] = 0.0 );
  matclassetaille := b;
end;

function  test_matmultisite(n,p : integer;a : rmat_type) : boolean;
{ check if a is a multisite nxn matrix whose blocks are Leslie or extended Leslie }
{ p is the number of sites }
var  i,j,m,ig,jg,k : integer;
     b,b1,b2 : boolean;
     g : rmat_type;
begin
  m := n div p; { nb of age classes of subpopulations }
  b := true;
  b1 := true;
  b2 := true;
  { check if diagonal blocks are Leslie of extended Leslie: }
  for k := 1 to p do
    begin
      ig := 0;
      for i := (k-1)*m + 1 to k*m do
        begin
          ig := ig + 1;
          jg := 0;
          for j := (k-1)*m + 1 to k*m do
            begin
              jg := jg + 1;
              g[ig,jg] := a[i,j];
            end;
        end;
      b1 := b1 and matleslie(m,g);
      b2 := b2 and matleslie2(m,g);
    end;
  { check if nondiagonal blocks have entries that are in [0,1] (migration rates): }
  b := b and (b1 or b2);
  for k := 1 to p do
    for i := (k-1)*m + 1 to k*m do
      for j := k*m + 1 to n do
        begin
          b := b and ( a[i,j] >= 0.0 ) and ( a[i,j] <= 1.0 );
          b := b and ( a[j,i] >= 0.0 ) and ( a[j,i] <= 1.0 );
        end;
  test_matmultisite := b;
end;

function  matmultisite(n : integer;a : rmat_type;var p : integer) : boolean;
{ a is a nxn multisite matrix whose blocks are Leslie or extended leslie }
{ return the number p of sites }
label 1;
var q : integer;
begin
  { look for p divisor of n }
  p := 1;
  matmultisite := false;
  q := 1;
1:
  q := q + 1;
  if ( n div q < 2 ) then exit;
  if ( n mod q <> 0 ) then goto 1;
  if test_matmultisite(n,q,a) then
    begin
      p := q;
      matmultisite := true;
      exit;
    end;
  goto 1;
end;

function  mattimedep(x : integer) : boolean;
{ check if matrix of index x is time dependent }
var i,j : integer;
    b : boolean;
begin
  b := false;
  with mat[x] do
  for i := 1 to size do
    for j := 1 to size do
      b := b or occur2(exp[i,j],exp_type[i,j],xtime);
  mattimedep := b;
end;

function  matvecdep(x,y : integer) : boolean;
{ check if matrix of index x is vector dependent (= density dependent) }
var i,j,k : integer;
    b : boolean;
begin
  b := false;
  with mat[x] do
  for i := 1 to size do
    for j := 1 to size do 
      for k := 1 to size do 
        b := b or occur2(exp[i,j],exp_type[i,j],vec[y].exp[k]);
  matvecdep := b;
end;

function  matrandom(x : integer): boolean;
{ check if matrix of index x is random (its entries depend on random functions) }
var i,j,z,tz : integer;
    b : boolean;
begin
  b := false;
  with mat[x] do
  for i := 1 to size do
    for j := 1 to size do
      begin 
        z  := exp[i,j];
        tz := exp_type[i,j];
        b  := b or occur_op1(z,tz,op1_rand);
        b  := b or occur_op1(z,tz,op1_gauss);
        b  := b or occur_op1(z,tz,op1_ber);
        b  := b or occur_op1(z,tz,op1_poisson);
        b  := b or occur_op1(z,tz,op1_geom);
        b  := b or occur_op1(z,tz,op1_expo);
        b  := b or occur_op1(z,tz,op1_gamm);
        b  := b or occur_fun(z,tz,fun_betaf);
        b  := b or occur_fun(z,tz,fun_beta1f);
        b  := b or occur_fun(z,tz,fun_binomf);
        b  := b or occur_fun(z,tz,fun_nbinomf);
        b  := b or occur_fun(z,tz,fun_nbinom1f);
        b  := b or occur_fun(z,tz,fun_gaussf);
        b  := b or occur_fun(z,tz,fun_lognormf);
        b  := b or occur_fun(z,tz,fun_tabf);
      end;
  matrandom := b;
end;

end.
