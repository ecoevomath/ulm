{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gopentemplate;

{$MODE Delphi}

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, LResources,
  gulm;

type

  tform_opentemplate = class(TForm)
    cancel_button : TBitBtn;
    create_button : TBitBtn;
    button_panel : TPanel;
    selector : TGroupBox;
    list : TListBox;
    description : TLabel;
    procedure FormCreate(Sender: TObject);
    procedure select_example(Sender: TObject; whom: Boolean);
    procedure cancel_buttonClick(Sender: TObject);
    procedure ok_buttonClick(Sender: TObject);
  private
  public
  end;

var form_opentemplate: tform_opentemplate;

implementation

{$R *.lfm}

procedure tform_opentemplate.FormCreate(Sender: TObject);
var
  w, h, i: integer;
begin
  w := list.Canvas.TextWidth('astrocaryum_mexicanum_0 ');
  h := list.Canvas.TextHeight('X');
  list.Constraints.MinWidth := w;
  list.Constraints.MinHeight := 12 * h;
  list.Constraints.MaxHeight := Trunc(Screen.Height / 2);
  selector.Constraints.MinWidth := 3 * w;
  for i := 0 to LazarusResources.count-1 do
    list.Items.Add(LazarusResources.Items[i].name);
end;

procedure tform_opentemplate.select_example(Sender: TObject; whom: Boolean);
{ Display the preview of the model when its selected in the list }
var
   r : TLResource;
   modelfile : String;
   lines : TStrings;
   i, k : integer;
begin
   description.Caption := '';
   if list.ItemIndex <> -1 then
      begin
	 lines := TStringList.Create();
	 modelfile := list.Items[list.ItemIndex];
	 r := LazarusResources.Find(modelfile);
	 if r <> nil then
	    begin
	       ExtractStrings([], [], PChar(r.Value), lines, True);
	       i := 0;
	       while (i < lines.Count-1) and
                     (length(lines[i]) > 1) and (lines[i][1] = #123) do
		  begin
		    k := length(lines[i]) - 1;
		    description.Caption := description.Caption +
                      Copy(lines[i], 3, k) + ' ';
		    i := i + 1;
		  end;
	    end;
      end;
end;

procedure tform_opentemplate.cancel_buttonClick(Sender: TObject);
begin
  Close;
end;

procedure tform_opentemplate.ok_buttonClick(Sender: TObject);
var
   modelfile : String;
begin
   if list.ItemIndex <> -1 then
      begin
	 modelfile := list.Items[list.ItemIndex];
	 form_ulm.fileloadExample(modelfile);
      end;
   Close;
end;

end.
