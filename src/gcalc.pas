{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gcalc;

{$MODE Delphi}

{  @@@@@@   form for online desk calculator   @@@@@@  }

interface

uses
  SysUtils, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type

  { tform_calc }

  tform_calc = class(TForm)
    Panel1: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    Memo1: TMemo;
    procedure Edit1EditingDone(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var  form_calc: tform_calc;

implementation

uses jglobvar,jsyntax,jeval;

{$R *.lfm}

procedure tform_calc.FormCreate(Sender: TObject);
begin
  memo1.Clear;
end;

procedure tform_calc.Edit1EditingDone(Sender: TObject);
var s : string;
    v,tv : integer;
    val : extended;
begin
  s := propre(minuscule(edit1.Text));
  if ( s = '' ) then exit;
  lirexp(s,v,tv); { read expression, check syntax }
  if err_syntax then
    begin
      err_syntax := false;
      exit;
    end;
  val := eval(v,tv); { evaluate expression }
  if err_eval then
    begin
      err_eval := false;
      exit;
    end;
  memo1.Lines.Append(''); { display result }
  memo1.Lines.Append(s);
  memo1.Lines.Append('-> ' + FloatToStr(val));
  edit1.Clear;
end;

procedure tform_calc.FormShow(Sender: TObject);
begin
  edit1.Clear;
  memo1.Clear;
end;

end.
