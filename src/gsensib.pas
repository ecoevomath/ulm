{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gsensib;

{  @@@@@@   form for computing sensitivities of matrix model   @@@@@@  }

{$MODE Delphi}

interface

uses SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,ExtCtrls,Grids,
     jglobvar,jmath;

type
  tform_sensib = class(TForm)
    top_panel: TPanel;
    main_panel: TPanel;
    sensit_bottom_panel: TPanel;
    elast_bottom_panel: TPanel;
    Label_sens_x: TLabel;
    Label_mat: TLabel;
    Label_lambda: TLabel;
    Label_sens: TLabel;
    Label_elas: TLabel;
    Label_sens_x_tot: TLabel;
    Label_elas_x_tot: TLabel;
    StringGrid_sens: TStringGrid;
    StringGrid_elas: TStringGrid;
    Edit_sens_x: TEdit;
    Edit_mat: TEdit;
    Edit_lambda: TEdit;
    Edit_sens_x_tot: TEdit;
    Edit_elas_x_tot: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure init_sensib;
    procedure erreur(s : string);
    procedure Edit_sens_xReturnPressed(Sender: TObject);
    procedure Edit_matReturnPressed(Sender: TObject);
    procedure eigenval(m : integer; var err : boolean);
    procedure eigenvec(m : integer; var err : boolean);
    procedure sensibilites(m,x : integer);
    procedure sensibilites_interp(m,x : integer);
    procedure sens_lambda1(m : integer; var err : boolean);
    procedure sens_lambda1_x(m,x : integer; var err : boolean);
  private
    x_mat : integer;    { pointer to matrix }
    x_var : integer;    { pointer to variable for sensitivity to lower level parameter }
    lambda : cvec_type; { complex eigenvalues }
    v,w : rvec_type;    { left and right eigenvectors associated with dominant eigenvalue }
    lambda1 : extended; { dominant eigenvalue }
    mat_sens : rmat_type; { matrix of sensitivities }
    mat_elas : rmat_type; { matrix of elasticities }
    sens_x_tot,elas_x_tot : extended; { total sensitivity/elasticity of lower level parameter }
    var_lambda,cv_lambda : extended; { variance and coeff of variation of dominant eigenvalue }
  public
  end;

var form_sensib: tform_sensib;

implementation

uses jutil,jsymb,jsyntax,jeval;

{$R *.lfm}

procedure tform_sensib.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Sensitivities - ' + s);
end;

procedure tform_sensib.FormCreate(Sender: TObject);
begin
  x_mat := 0;
  x_var := 0;
end;

procedure tform_sensib.eigenval(m : integer;var err : boolean);
{ eigenvalues of matrix pointed by m }
begin
  err := true;
  with mat[m] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then
        begin
          erreur('Dominant eigenvalue <= 0');
          exit;
        end;
    end;
  err := false;
end;

procedure tform_sensib.eigenvec(m : integer; var err : boolean);
{ eigenvectors associated with dominant eigenvalue of matrix m }
var i : integer;
    vv,ww : cvec_type;
begin
  err := true;
  with mat[m] do
    begin
      matvecpropdroite(size,val,lambda[1],ww); { right eigenvector }
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w); { sum of vector entries = 1 }
      matvecpropgauche(size,val,lambda[1],vv); { left eigenvector }
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do v[i] := vv[i].re;
      vecnormalise1(size,v); { sum of vector entries = 1 }
    end;
  err := false;
end;

procedure tform_sensib.sens_lambda1(m : integer;var err : boolean);
var i,j : integer;
    r : extended;
begin
  err := true;
  with mat[m] do
    begin
      r := vecpscal(size,v,w);
      if ( r = 0.0 ) then exit;
      { matrix of sensitivities: }
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/r;
      { matrix of elasticities: }
      for i := 1 to size do
        for j := 1 to size do
          mat_elas[i,j] := val[i,j]*mat_sens[i,j]/lambda1;
      r := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          if ( val[i,j] > 0.0 ) then
            r := r + sqr(mat_sens[i,j]);
      var_lambda := r; { variance of dominant eigenvalue }
      r := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          r := r + sqr(mat_elas[i,j]);
      cv_lambda := sqrt(r); { coeff of variaton of dominant eigenvalue }
    end;
  err := false;
end;

procedure tform_sensib.sens_lambda1_x(m,x : integer; var err : boolean);
{ sensitivity of lambda1 to changes in lower level parameter x of matrix m }
{ use the chain rule }
var i,j,y,ty : integer;
    r,som : extended;
begin
  err := true;
  with mat[m] do
    begin
      r := vecpscal(size,v,w);
      if ( r = 0.0 ) then exit;
      { matrix of sensitivities: }
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/r;
      som := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            { derivative of matrix entry with respect to lower level parameter x }
            deriv(exp[i,j],exp_type[i,j],x,y,ty);
            if err_eval then
              begin
                err_eval := false;
                exit;
              end;
            r := eval(y,ty);
            mat_sens[i,j] := mat_sens[i,j]*r;
            mat_elas[i,j] := variable[x].val*mat_sens[i,j]/lambda1;
            som := som + mat_sens[i,j];
          end;
      sens_x_tot := som; { total sensitivity }
      elas_x_tot := som*variable[x].val/lambda1; { total elasticity }
    end;
  err := false;
end;

procedure tform_sensib.sensibilites(m,x : integer);
{ compute sensitivities and elasticities; matrix m, lower level parameter x }
var i,j : integer;
    err : boolean;
begin
  with mat[m] do
    begin
      edit_mat.Text := s_ecri_mat(m);
      eigenval(m,err); { eigenvalues }
      if err then exit;
      eigenvec(m,err); { eigenvectors }
      if err then exit;
      edit_lambda.Text := Format('%1.6g',[lambda1]);
      if ( x = 0 ) then
        begin
          edit_sens_x.Text := '';
          edit_sens_x_tot.Text := '';
          edit_elas_x_tot.Text := '';
          sens_lambda1(m,err); { sensitivities of matrix entries }
          if err then exit;
        end
      else
        begin
          edit_sens_x.Text := s_ecri_var(x);
          sens_lambda1_x(m,x,err); { sensitivities of lower level parameter x }
          if err then exit;
          edit_sens_x_tot.Text := Format('%10.4g',[sens_x_tot]);
          edit_elas_x_tot.Text := Format('%10.4g',[elas_x_tot]);
        end;
      with stringgrid_sens do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := size + 1;
          RowCount  := size + 1;
          for i := 1 to ColCount-1 do Cells[i,0] := IntToStr(i);
          for i := 1 to RowCount-1 do Cells[0,i] := IntToStr(i);
          for i := 1 to size do
            for j := 1 to size do
              Cells[j,i] := Format('%10.4f',[mat_sens[i,j]]);
          AutoSizeColumns(); 
        end;
      with stringgrid_elas do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := size + 1;
          FixedCols := 1;
          RowCount  := size + 1;
          FixedRows := 1;
          for i := 1 to size do Cells[i,0] := IntToStr(i);
          for i := 1 to size do Cells[0,i] := IntToStr(i);
          for i := 1 to size do
            for j := 1 to size do
              Cells[j,i] := Format('%10.4f',[mat_elas[i,j]]);
          AutoSizeColumns(); 
        end;
    end;
end;

procedure tform_sensib.sensibilites_interp(m,x : integer);
{ results that appear in the main window under the sensitivity command }
{ see procedure interp_sensibilite in unit jinterp.pas }
var i,j : integer;
    err : boolean;
    s : string;
begin
  with mat[m] do
    begin
      iwriteln('Matrix ' + s_ecri_mat(m));
      eigenval(m,err);
      if err then exit;
      eigenvec(m,err);
      if err then exit;
      iwriteln('  Lambda = ' + Format('%1.6g',[lambda1]));
      if ( x = 0 ) then
        begin
          sens_lambda1(m,err);
          if err then exit;
          iwriteln('Assuming a same variance v on non-zero matrix entries:');
          iwriteln('  Var(lambda) =  ' + Format('%1.4g',[var_lambda]) + '*v');
          iwriteln('Assuming a same coeff. of variation c on matrix entries:');
          iwriteln('   CV(lambda) =  ' + Format('%1.4g',[cv_lambda]) + '*c');
          iwriteln('Sensitivities:');
          for i := 1 to size do
            begin
              s := '';
              for j := 1 to size do s := s + Format('%10.4f',[mat_sens[i,j]]);
              iwriteln(s);
            end;
          iwriteln('Elasticities:');
          for i := 1 to size do
            begin
              s := '';
              for j := 1 to size do s := s + Format('%10.4f',[mat_elas[i,j]]);
              iwriteln(s);
            end;
        end
      else
        begin
          sens_lambda1_x(m,x,err);
          if err then exit;
          iwriteln('Sensitivity lambda to ' + s_ecri_var(x) + ' = ' + Format('%1.4g',[sens_x_tot]));
          iwriteln('Elasticity lambda to ' + s_ecri_var(x) + ' = ' + Format('%1.4g',[elas_x_tot]));
        end;
    end;
end;

procedure tform_sensib.FormActivate(Sender: TObject);
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      edit_mat.Text := '';
      edit_lambda.Text := '';
      exit;
    end;
  sensibilites(x_mat,x_var);
  AutoSize := False;
end;

procedure Tform_sensib.init_sensib;
var i : integer;
begin
  init_form(form_sensib);
  x_mat := 0;
  x_var := 0;
  for i := 1 to modele_nb do
    if ( modele[i].xmat <> 0 ) then
      begin
        x_mat := modele[i].xmat;
        exit;
      end;
end;

procedure Tform_sensib.Edit_matReturnPressed(Sender: TObject);
{ enter name of matrix }
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Matrix: unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

procedure Tform_sensib.Edit_sens_xReturnPressed(Sender: TObject);
{ enter name of variable }
var x : integer;
    s : string;
begin
  s := edit_sens_x.Text;
  if ( s = '' ) then
    begin
      x_var := 0;
      FormActivate(nil);
      exit;
    end;
  if not test_variable(edit_sens_x.Text,x) then
    begin
      erreur('Variable: unknown variable name');
      edit_sens_x.Text := s_ecri_var(x_var);
      exit;
    end;
  x_var := x;
  FormActivate(nil);
end;

end.
