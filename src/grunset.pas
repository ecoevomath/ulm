{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit grunset;

{$MODE Delphi}

{  @@@@@@   form for setting run and montecarlo parameters   @@@@@@  }

interface

uses
  SysUtils, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type

  { Tform_runset }

  Tform_runset = class(TForm)
    label_keymap_info: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    button_panel: TPanel;
    ok_button: TBitBtn;
    cancel_button: TBitBtn;
    apply_button: TBitBtn;
    GroupBox1: TGroupBox;
    graine_edit: TEdit;
    label_graine: TLabel;
    GroupBox2: TGroupBox;
    nb_cycle_carlo_edit: TEdit;
    label_nb_cycle_carlo: TLabel;
    nb_traj_carlo_edit: TEdit;
    label_nb_traj_carlo: TLabel;
    ext_carlo_edit: TEdit;
    label_ext_carlo: TLabel;
    esc_carlo_edit: TEdit;
    label_esc_carlo: TLabel;
    GroupBox3: TGroupBox;
    nb_cycle_edit: TEdit;
    dt_texte_edit: TEdit;
    label_nb_cycle: TLabel;
    label_dt_texte: TLabel;
    procedure ok_buttonClick(Sender: TObject);
    procedure cancel_buttonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure apply_buttonClick(Sender: TObject);
  private
    err_runset : boolean;
    procedure erreur(s : string);
  public
  end;

var form_runset: tform_runset;

implementation

uses jglobvar,jutil,jmath,jsyntax,jinterp,gulm;

{$R *.lfm}

procedure tform_runset.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Run settings - ' + s);
  err_runset := true;
end;

procedure tform_runset.ok_buttonClick(Sender: TObject);
begin
  apply_buttonClick(nil);
  if not err_runset then Close;
end;

procedure tform_runset.apply_buttonClick(Sender: TObject);
var nb_cycle1,dt_texte_interp1,graine1,
    nb_cycle_carlo1,nb_run_carlo1 : integer;
    seuil_ext1,seuil_div1 : extended;
    initinit : boolean;
begin
  err_runset := false;
  initinit := false;
  if not est_entier(nb_cycle_edit.Text,nb_cycle1) then
    begin
      erreur('Number of time steps: positive integer expected');
      exit;
    end
  else
    if ( nb_cycle1 <= 0 ) then
      begin
        erreur('Number of time steps: positive integer expected');
        exit;
      end;
  if not est_entier(dt_texte_edit.Text,dt_texte_interp1) then
    begin
      erreur('Sampling interval for text interp: positive integer expected');
      exit;
    end
  else
    if ( dt_texte_interp1 < 1 ) then
      begin
        erreur('Sampling interval for text interp: positive integer expected');
        exit;
      end;
  if not est_entier(graine_edit.Text,graine1) then
    begin
      erreur('Random generator seed: positive integer expected');
      exit;
    end;
  if ( graine1 < 0 ) then
    begin
      erreur('Random generator seed: positive integer expected');
      exit;
    end;
  if ( graine1 <> (graine0 - graine00 + 1) ) then initinit := true;
  if not est_entier(nb_cycle_carlo_edit.Text,nb_cycle_carlo1) then
    begin
      erreur('Number of time steps MonteCarlo: positive integer expected');
      exit;
    end
  else
    if ( nb_cycle_carlo1 <= 0 ) then
      begin
        erreur('Number of time steps MonteCarlo: positive integer expected');
        exit;
      end;
  if not est_entier(nb_traj_carlo_edit.Text,nb_run_carlo1) then
    begin
      erreur('Number of trajectories MonteCarlo: positive integer expected');
      exit;
    end
  else
    if ( nb_run_carlo1 <= 0 ) then
      begin
        erreur('Number of trajectories MonteCarlo: positive integer expected');
        exit;
      end;
  if not est_reel(ext_carlo_edit.Text,seuil_ext1) then
    begin
      erreur('Extinction threshold MonteCarlo: positive number expected');
      exit;
    end
  else
    if ( seuil_ext1 <= 0.0 ) then
      begin
        erreur('Extinction threshold MonteCarlo: positive number expected');
        exit;
      end;
  if not est_reel(esc_carlo_edit.Text,seuil_div1) then
    begin
      erreur('Escape threshold MonteCarlo: positive number expected');
      exit;
    end
  else
    if ( seuil_div1 <= 0.0 ) then
      begin
        erreur('Escape threshold MonteCarlo: positive number expected');
        exit;
      end;
  if ( seuil_div1 <= seuil_ext1 ) then
    begin
      erreur('Escape threshold <= extinction threshold');
      exit;
    end;
  nb_cycle := nb_cycle1;
  dt_texte_interp := dt_texte_interp1;
  nb_cycle_carlo  := nb_cycle_carlo1;
  nb_run_carlo    := nb_run_carlo1;
  seuil_ext := seuil_ext1;
  seuil_div := seuil_div1;
  if initinit then { change random generator seed }
    begin
      graine0 := graine00 + (graine1-1);
      if ( graine1 <= 0 ) then graine0 := graine00;
      graine := graine0;
      form_ulm.run_initExecute(nil);
    end;
end;

procedure tform_runset.cancel_buttonClick(Sender: TObject);
begin
  Close;
end;

procedure tform_runset.FormCreate(Sender: TObject);
begin
end;

procedure Tform_runset.FormActivate(Sender: TObject);
begin
  with form_ulm do
    begin
      nb_cycle_edit.Text := IntToStr(nb_cycle);
      dt_texte_edit.Text := IntToStr(dt_texte_interp);
      graine_edit.Text   := IntToStr(graine0-graine00+1);
      nb_cycle_carlo_edit.Text := IntToStr(nb_cycle_carlo);
      nb_traj_carlo_edit.Text  := IntToStr(nb_run_carlo);
      ext_carlo_edit.Text := Format('%1.1f',[seuil_ext]);
      esc_carlo_edit.Text := Format('%1.0f',[seuil_div]);
    end;
  AutoSize := False;
end;

end.
