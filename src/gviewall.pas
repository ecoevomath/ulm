{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gviewall;

{$MODE Delphi}

{ @@@@@@  form to display internal objects: matrices, vectors, relations, variables, functions  @@@@@@ }

interface

uses
  SysUtils, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Menus, ExtCtrls, StdCtrls;

type
  tform_view_all = class(TForm)
    TreeView1: TTreeView;
    Memo1: TMemo;
    procedure tree;
    procedure display;
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure init_viewall;
  end;

var  form_view_all: tform_view_all;

implementation

uses jglobvar,jsymb,jsyntax;

{$R *.lfm}

procedure tform_view_all.tree;
{ construct the tree of all objects }
var treenode,tn : TTreeNode;
    z,x,i : integer;
begin
  with treeview1.Items do
    begin
      treenode := nil;
      for z := 1 to modele_nb do with modele[z] do
        begin
          treenode := Add(treenode,s_ecri_model(z));
          if ( xmat <> 0 ) then
            begin
              AddChild(treenode,s_ecri_mat(xmat));
              AddChild(treenode,s_ecri_vec(xvec));
            end
          else
            begin
              AddChild(treenode,'Relations');
              tn := treenode.GetLastChild;
              for i := 1 to size do
                AddChild(tn,s_ecri_rel(xrel[i]));
            end;
        end;
      if ( fun_nb - fun_nb_predef > 0 ) then
        begin
          treenode := Add(treenode,'Functions');
          for x := fun_nb_predef+1 to fun_nb do
            AddChild(treenode,s_ecri_fun(x));
        end;
      i := 0;
      for x := 1 to rel_nb do with rel[x] do
        if ( xmodele = 0 ) then i := i + 1;
      if ( i > 0 ) then
        begin
          treenode := Add(treenode,'Other relations');
          for x := 1 to rel_nb do with rel[x] do
            if ( xmodele = 0 ) then
              AddChild(treenode,s_ecri_rel(x));
        end;
      treenode := Add(treenode,'Variables');
      for x := 1 to variable_nb do
        AddChild(treenode,s_ecri_var(x));
    end;
end;

procedure tform_view_all.display;
{ display the tree of all objects in the left part of the form }
{ selected objects are displayed in the right part of the form }
var x,tx,z,tz : integer;
    s : string;
begin
  with treeview1 do
    if Visible and Assigned(Selected) then
      begin
        s := Selected.Text;
        if ( s = 'Variables' ) then b_ecri_list_variable
        else
        if ( s = 'Relations' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_list_rel_model(z);
          end
        else
        if ( s = 'Functions' ) then b_ecri_list_fun
        else
        if ( s = 'Other relations') then b_ecri_list_rel_indep
        else
        if ( s = 'Matrix' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_mat(modele[z].xmat);
          end
        else
        if ( s = 'Vector' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_vec(modele[z].xvec);
          end
        else
          begin
            trouve_obj(s,x,tx);
            b_ecri(x,tx);
          end;
        memo1.Clear;
        memo1.Lines := lines_syntax;
      end;
end;

procedure tform_view_all.TreeView1Change(Sender: TObject; Node: TTreeNode);
begin
  display;
end;

procedure tform_view_all.FormCreate(Sender: TObject);
begin
  memo1.ReadOnly := true;
  memo1.Clear;
end;

procedure tform_view_all.FormActivate(Sender: TObject);
begin
  display;
  AutoSize := False;
end;

procedure tform_view_all.init_viewall;
begin
  treeview1.Items.Clear;
  tree;
  with treeview1 do Selected := Items[1];
  display;
end;

end.
