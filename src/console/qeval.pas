{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit qeval;

{  @@@@@@   evaluateur   @@@@@@  }

interface

uses qglobvar;

procedure deriv(x,tx,xd : integer;var y,ty : integer);
function  occur2(x,tx,y : integer) : boolean;
function  occur_op1(x,tx,y : integer) : boolean;
function  occur_fun(x,tx,y : integer) : boolean;
function  eval(x,tx : integer) : extended;
procedure eval_mat(x : integer);
procedure init_eval;
procedure init_eval1;
procedure run_t;

var    ordre_eval    : array[1..variable_nb_max] of integer;
       ordre_eval_nb : integer;
       err_eval : boolean;

implementation

uses   qsymb,qsyntax,qmath;

const  succ_nb_max = 100;

type   aggg = array[1..variable_nb_max] of
                record
                  succ_nb : integer;
                  succ    : array[1..succ_nb_max] of integer;
                end;

var    ggg : aggg;

procedure erreur_eval(s : string);
begin
  writeln('Error: Eval - ',s);
  err_eval := true;
end;

{ ----- derivees ----- }

procedure deriv_op2(op,u,tu,v,tv,xd : integer;var y,ty : integer);
var du,tdu,dv,tdv,z1,z2,xx : integer;
begin
  deriv(u,tu,xd,du,tdu);
  if ( tdu = type_lis ) then push(du);
  deriv(v,tv,xd,dv,tdv);
  if ( tdv = type_lis ) then push(dv);
  ty := type_lis;
  case op of 
    op2_plus    :  begin  {  d(u + v) = d(u) + d(v)  }
                     if ( tdu = type_ree ) and ( du = ree_zero ) then  { d(v) }
                       begin
                         y  := dv;
                         ty := tdv;
                       end
                     else
                     if ( tdv = type_ree ) and ( dv = ree_zero ) then  { d(u) }
                       begin
                         y  := du;
                         ty := tdu;
                       end
                     else
                       y  := cons(op2_plus,type_op2,cons(du,tdu,list(dv,tdv)));
                   end;
    op2_mult    :  begin  {  d(u*v) = u*d(v) + v*d(u)  }
                     if ( tdu = type_ree ) and ( du = ree_zero ) then  { u*d(v) }
                       y := cons(op2_mult,type_op2,cons(u,tu,list(dv,tdv)))
                     else
                     if ( tdv = type_ree ) and ( dv = ree_zero ) then  { v*d(u) }
                       y := cons(op2_mult,type_op2,cons(v,tv,list(du,tdu)))
                     else
                       begin
                         z1 := cons(op2_mult,type_op2,cons(u,tu,list(dv,tdv)));
                         push(z1);
                         z2 := cons(op2_mult,type_op2,cons(v,tv,list(du,tdu)));
                         y  := cons(op2_plus,type_op2,cons(z1,type_lis,list(z2,type_lis)));
                         xx := pop;
                       end; 
                   end;
    op2_moins   :  begin  {  d(u - v) = d(u) - d(v)  }
                     if ( tdu = type_ree ) and ( du = ree_zero ) then  { -d(v) }
                       begin
                         y  := cons(op1_moins,type_op1,list(dv,tdv));
                       end
                     else
                     if ( tdv = type_ree ) and ( dv = ree_zero ) then  { d(u) }
                       begin
                         y  := du;
                         ty := tdu;
                       end
                     else
                       y  := cons(op2_moins,type_op2,cons(du,tdu,list(dv,tdv)));
                  end;
    op2_div     :  begin  {  d(u/v) = (v*d(u) - u*d(v)) / (v*v)  }
                     if ( tdu = type_ree ) and ( du = ree_zero ) then  { -u*d(v)/(v*v) }
                       begin
                         z1 := cons(op2_mult,type_op2,cons(u,tu,list(dv,tdv)));
                         z1 := cons(op1_moins,type_op1,list(z1,type_lis));
                         push(z1);
                         z2 := cons(op2_mult,type_op2,cons(v,tv,list(v,tv))); 
                         y  := cons(op2_div,type_op2,cons(z1,type_lis,list(z2,type_lis)));
                         xx := pop;
                       end
                     else
                     if ( tdv = type_ree ) and ( dv = ree_zero ) then { d(u)/v }
                       begin
                         y  := cons(op2_div,type_op2,cons(du,tdu,list(v,tv)));
                       end
                     else
                       begin
                         z1 := cons(op2_mult,type_op2,cons(v,tv,list(du,tdu)));
                         push(z1);
                         z2 := cons(op2_mult,type_op2,cons(u,tu,list(dv,tdv)));
                         push(z2);
                         z1 := cons(op2_moins,type_op2,cons(z1,type_lis,list(z2,type_lis)));
                         z2 := cons(op2_mult,type_op2,cons(v,tv,list(v,tv))); 
                         xx := pop;
                         push(z2);
                         y  := cons(op2_div,type_op2,cons(z1,type_lis,list(z2,type_lis)));
                         xx := pop;
                         xx := pop;
                       end;
                   end;
    op2_puis    :  begin  {  d(u^v) = v*d(u)*u^(v-1) + d(v)*ln(u)*u^v  }
                     if ( tdv = type_ree ) and ( dv = ree_zero ) then { v*d(u)*u^(v-1) }
                       begin
                         z1 := cons(op2_mult,type_op2,cons(v,tv,list(du,tdu)));
                         push(z1);
                         if ( tv = type_ree ) then
                           begin
                             v  := cre_ree(ree[v].val-1.0);
                             z2 := cons(op2_puis,type_op2,cons(u,tu,list(v,tv)));
                           end
                         else
                           begin
                             z2 := cons(op2_moins,type_op2,cons(v,tv,list(ree_un,type_ree)));
                             z2 := cons(op2_puis,type_op2,cons(u,tu,list(z2,type_lis)));
                           end;
                         y  := cons(op2_mult,type_op2,cons(z1,type_lis,list(z2,type_lis)));
                         xx := pop;
                       end
                     else
                     if ( tdu = type_ree ) and ( du = ree_zero ) then { d(v)*ln(u)*u^v }
                       begin
                         z1 := cons(op1_ln,type_op1,list(u,tu));
                         z1 := cons(op2_mult,type_op2,cons(dv,tdv,list(z1,type_lis)));
                         push(z1);
                         z2 := cons(op2_puis,type_op2,cons(u,tu,list(v,tv)));
                         y := cons(op2_mult,type_op2,cons(z1,type_lis,list(z2,type_lis)));
                         xx := pop;
                       end
                     else
                       begin
                         z1 := cons(op2_mult,type_op2,cons(v,tv,list(du,tdu)));
                         push(z1);
                         z2 := cons(op2_moins,type_op2,cons(v,tv,list(ree_un,type_ree)));
                         z2 := cons(op2_puis,type_op2,cons(u,tu,list(z2,type_lis)));
                         push(z2);
                         y  := cons(op2_mult,type_op2,cons(z1,type_lis,list(z2,type_lis)));
                         push(y);
                         z1 := cons(op1_ln,type_op1,list(u,tu));
                         z1 := cons(op2_mult,type_op2,cons(dv,tdv,list(z1,type_lis)));
                         push(z1);
                         z2 := cons(op2_puis,type_op2,cons(u,tu,list(v,tv)));
                         push(z2);
                         z2 := cons(op2_mult,type_op2,cons(z1,type_lis,list(z2,type_lis)));
                         push(z2);
                         y  := cons(op2_plus,type_op2,cons(y,type_lis,list(z2,type_lis)));
                         xx := pop;
                         xx := pop;
                         xx := pop;
                         xx := pop;
                         xx := pop;
                         xx := pop;
                       end;
                  end;
    else
      begin
        y := 0;
        erreur_eval('binary operator unknown for differentiation');
      end;
  end;
  if ( tdv = type_lis ) then xx := pop;
  if ( tdu = type_lis ) then xx := pop;
end;

procedure deriv_op1(op,u,tu,xd : integer;var y,ty : integer);
var du,tdu,xx : integer;
begin
  deriv(u,tu,xd,du,tdu);
  if ( tdu = type_ree ) and ( du = ree_zero ) then
    begin
      y  := ree_zero;
      ty := type_ree;
      exit;
    end;
  ty := type_lis;
  if ( tdu = type_lis ) then push(du);
  case op of 
    op1_moins   :  begin  {  d(-u) = -d(u)  }
                     y  := cons(op1_moins,type_op1,list(du,tdu));
                   end;
    op1_cos     :  begin  {  d(cos(u)) = -sin(u)*d(u)  }
                     y := cons(op1_sin,type_op1,list(u,tu));
                     y := cons(op1_moins,type_op1,list(y,type_lis));
                     if ( tdu = type_ree ) and ( du = ree_un ) then
                     else
                       begin
                         push(y);
                         y := cons(op2_mult,type_op2,cons(y,type_lis,list(du,tdu)));
                         xx := pop;
                       end;
                   end;
    op1_sin     :  begin  {  d(sin(u)) = cos(u)*d(u)  }
                     y := cons(op1_cos,type_op1,list(u,tu));
                     if ( tdu = type_ree ) and ( du = ree_un ) then
                     else
                       begin
                         push(y);
                         y := cons(op2_mult,type_op2,cons(y,type_lis,list(du,tdu)));
                         xx := pop;
                       end;
                   end;
    op1_tan     :  begin  {  d(tan(u)) = d(u)/(cos(u)^2)  }
                     y := cons(op1_cos,type_op1,list(u,tu));
                     push(y);
                     y := cons(op2_mult,type_op2,cons(y,type_lis,list(y,type_lis))); 
                     y := cons(op2_div,type_op2,cons(du,tdu,list(y,type_lis)));
                     xx := pop;
                   end;
    op1_atan    :  begin  {  d(atan(u)) = d(u)/(1 + u^2)  }
                     y := cons(op2_mult,type_op2,cons(u,tu,list(u,tu))); 
                     push(y);
                     y := cons(op2_plus,type_op2,cons(ree_un,type_lis,list(y,type_lis)));
                     y := cons(op2_div,type_op2,cons(du,tdu,list(y,type_lis)));
                     xx := pop;
                   end;
    op1_ln     :  begin  {  d(ln(u)) = d(u)/u  }
                     y := cons(op2_div,type_op2,cons(du,tdu,list(u,tu)));
                   end;
    op1_log    :  begin  {  d(log(u)) = d(u)/u/ln(10)  }
                     y := cons(op2_div,type_op2,cons(du,tdu,list(u,tu)));
                     push(y);
                     y := cons(op2_div,type_op2,cons(cre_ree(ln(10.0)),type_ree,list(y,type_lis)));
                     xx := pop;
                   end;
    op1_exp     :  begin  {  d(exp(u)) = d(u)*exp(u)  }
                     y := cons(op1_exp,type_op1,list(u,tu));
                     y := cons(op2_mult,type_op2,cons(du,tdu,list(y,type_lis)));
                   end;
    op1_sqrt    :  begin  {  d(sqrt(u)) = d(u)/(2*sqrt(u))  }
                     y := cons(op1_sqrt,type_op1,list(u,tu));
                     y := cons(op2_mult,type_op2,cons(ree_deux,type_ree,list(y,type_lis)));
                     y := cons(op2_div,type_op2,cons(du,tdu,list(y,type_lis)));
                   end;
    else
      begin
        y := 0 ;
        erreur_eval('unary operator unknown for differentiation');
      end;
  end;
  if ( tdu = type_lis ) then xx := pop;
end;

procedure deriv_lis(x,xd : integer;var y,ty : integer);
var op,top,z,u,tu,v,tv,xx : integer;
begin
  if ( x = 0 ) then
    begin
      y  := x;
      ty := type_lis;
      exit;
    end;
  with lis[x] do
    begin
      op  := car;
      top := car_type;
      case top of
      type_op1 : begin
                   z  := cdr;
                   tu := lis[z].car_type;
                   u  := lis[z].car;
                   if ( tu = type_lis ) then push(u);
                   deriv_op1(op,u,tu,xd,y,ty);
                   if ( tu = type_lis ) then xx := pop;
                 end;
      type_op2 : begin
                   z  := cdr;
                   tu := lis[z].car_type;
                   u  := lis[z].car;
                   z  := lis[z].cdr;
                   tv := lis[z].car_type;
                   v  := lis[z].car;
                   if ( tu = type_lis ) then push(u);
                   if ( tv = type_lis ) then push(v);
                   deriv_op2(op,u,tu,v,tv,xd,y,ty);
                   if ( tv = type_lis ) then xx := pop;
                   if ( tu = type_lis ) then xx := pop;
                 end;
      type_fun : begin
                   erreur_eval('differentiation of functions not implemented');
                 end;
      else;
      end;
    end;
end;

function  occur2(x,tx,y : integer) : boolean;
{ regarde recursivement si la variable y a une occurence dans l'expression x  }
begin
 occur2 := false;
 case tx of
    type_lis : 
      while( x <> 0 ) do with lis[x] do
        begin
          if occur2(car,car_type,y ) then
            begin
              occur2 := true;
              exit;
            end;
           x := cdr;
        end;
    type_variable :
      if ( x = y ) then
        occur2 := true
      else
        occur2 := occur2(variable[x].exp,variable[x].exp_type,y);
    type_fun :
      occur2 := occur2(fun[x].exp,fun[x].exp_type,y);
    else;
  end;
end;

function  occur_op1(x,tx,y : integer) : boolean;
{ regarde si l'operateur unaire y a une occurence dans l'expression x  }
begin
  occur_op1 := false;
  case tx of
    type_lis : 
      while( x <> 0 ) do with lis[x] do
        begin
          if occur_op1(car,car_type,y) then
            begin
              occur_op1 := true;
              exit;
            end;
          x := cdr;
        end;
    type_variable :
      with variable[x] do occur_op1 := occur_op1(exp,exp_type,y);
    type_fun :
      with fun[x] do occur_op1 := occur_op1(exp,exp_type,y);
    type_op1 :
      if ( x = y ) then occur_op1 := true;
  end;
end;

function  occur_fun(x,tx,y : integer) : boolean;
{ regarde si la fonction y a une occurence dans l'expression x  }
begin
  occur_fun := false;
  case tx of
    type_lis : 
      while( x <> 0 ) do with lis[x] do
        begin
          if occur_fun(car,car_type,y) then
            begin
              occur_fun := true;
              exit;
            end;
          x := cdr;
        end;
    type_variable :
      with variable[x] do occur_fun := occur_fun(exp,exp_type,y);
    type_fun :
      if ( x = y ) then occur_fun := true;
    else;
  end;
end;

procedure deriv_variable(x,xd : integer;var y,ty : integer);
begin
  with variable[x] do
    if ( x = xd ) then
      begin
        y  := ree_un;
        ty := type_ree;
      end
    else
      if occur2(exp,exp_type,xd) then
        deriv(exp,exp_type,xd,y,ty)
      else
        begin
          y  := ree_zero;
          ty := type_ree;
        end;
end;

procedure deriv_ree(var y,ty : integer);
begin
  y  := ree_zero;
  ty := type_ree;
end;

procedure deriv(x,tx,xd : integer;var y,ty : integer);
begin
  case tx of
    type_lis       : deriv_lis(x,xd,y,ty);
    type_variable  : deriv_variable(x,xd,y,ty);
    type_ree       : deriv_ree(y,ty);
    else writeln('deriv? ');
  end;
end;

{ ------ evaluation des operateurs ------ }

function  eval_op1(op : integer;v : extended) : extended;
var w : extended;
begin
  eval_op1 := 0.0;
  case op of
    op1_moins   :  begin
                     eval_op1 := -v;
                   end;
    op1_cos     :  begin
                     eval_op1 := cos(v);
                   end;
    op1_sin     :  begin
                     eval_op1 := sin(v);
                   end;
    op1_tan     :  begin
                     w := cos(v);
                     if ( w <> 0.0 ) then
                       eval_op1 := sin(v)/w
                     else
                       begin
                         erreur_eval('operator argument: tan('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_atan    :  begin
                     eval_op1 := arctan(v);
                   end;
    op1_ln      :  begin
                     if ( v > 0.0 ) then 
                       eval_op1 := ln(v)
                     else
                       begin
                         erreur_eval('operator argument: ln('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_ln0     :  begin
                     eval_op1 := ln0(v)
                   end;
    op1_log     :  begin
                     if ( v > 0.0 ) then 
                       eval_op1 := log(v)
                     else
                       begin
                         erreur_eval('operator argument: log('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_exp     :  begin
                     eval_op1 := exp(v);
                   end;
    op1_fact    :  begin
                     eval_op1 := fact(round(v));
                   end;
    op1_sqrt    :  if ( v >= 0.0 ) then
                     eval_op1 := sqrt(v) 
                   else
                     begin
                       erreur_eval('operator argument: sqrt('+s_ecri_val(v)+')');
                       exit;
                     end;
    op1_abs     :  begin
                     eval_op1 := abs(v);
                   end;
    op1_trunc   :  begin
                     eval_op1 := trunc(v);
                   end;
    op1_round   :  begin
                     eval_op1 := round(v); 
                   end;
    op1_gauss   :  begin
                     if ( v >= 0.0 ) then
                       eval_op1 := gauss(0.0,v)
                     else
                       begin
                         erreur_eval('operator argument: gauss('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_rand    :  begin
                     eval_op1 := rand(abs(v));
                   end;
    op1_ber     :  begin
                     if ( v >= 0 ) and ( v <= 1) then
                       eval_op1 := ber(v)
                     else
                       begin
                         erreur_eval('operator argument: ber('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_gamm    :  begin
                     if ( v >= 0 ) then
                       eval_op1 := gamm(v)
                     else
                       begin
                         erreur_eval('operator argument: gamm('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_geom :  begin
                     if ( v >= 0 ) and ( v <= 1.0 ) then 
                       eval_op1 := geom(v)
                     else
                       begin
                         erreur_eval('operator argument: geom('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_expo :  begin
                     if ( v > 0 ) then
                       eval_op1 := exponential(v)
                     else
                       begin
                         erreur_eval('operator argument: expo('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_poisson :  begin
                     if ( v >= 0 ) then 
                       eval_op1 := poisson(v)
                     else
                       begin
                         erreur_eval('operator argument: poisson('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    else writeln('eval_op1?');
  end;
end;

function  eval_op2(op : integer;v,w : extended) : extended;
var n : integer;
begin
  eval_op2 := 0.0;
  case op of
    op2_plus    :  eval_op2 := v + w;
    op2_mult    :  eval_op2 := v * w;
    op2_moins   :  eval_op2 := v - w;
    op2_div     :  if w <> 0.0 then 
                     eval_op2 := v / w
                   else
                     begin
                       erreur_eval('divide by 0');
                       exit;
                     end;
    op2_puis    :  if ( v = 0.0 ) then
                       eval_op2 := 0.0
                   else
                   if ( trunc(w) = w ) then
                     begin
                       n := trunc(w);
                       if ( v < 0.0 ) then 
                          if ( n mod 2 = 0 ) then
                            eval_op2 := exp(w*ln(-v))
                          else
                            eval_op2 := -exp(w*ln(-v))
                       else
                         eval_op2 := exp(w*ln(v))
                     end
                   else
                     if ( v > 0.0 ) then
                       eval_op2 := exp(w*ln(v))
                     else
                       begin
                         erreur_eval('power of a negative number: '+s_ecri_val(v));
                         exit;
                       end;
    op2_infe    :  if v < w then 
                     eval_op2 := 1.0
                   else 
                     eval_op2 := 0.0;
    op2_supe    :  if v > w then 
                     eval_op2 := 1.0
                   else 
                     eval_op2 := 0.0;
    op2_mod     :  if w <> 0.0 then
                     eval_op2 := v - w*trunc(v/w)
                   else
                     begin
                       erreur_eval('divide by 0');
                       exit;
                     end;
    else writeln('eval_op2?');
  end;
end;

{ ------ evaluation des fonctions ------ }

function  eval_if(larg : integer) : extended;
var v : extended;
begin
  with lis[larg] do
    begin
      v := eval(car,car_type);
      larg := cdr;
    end;
  if ( v <> 0.0 ) then
    with lis[larg] do eval_if := eval(car,car_type)
  else
    begin
      larg := lis[larg].cdr;
      with lis[larg] do eval_if := eval(car,car_type);
    end;
end;

function  eval_min(larg : integer) : extended;
var v : extended;
begin
  v := maxextended;
  while ( larg <> 0 ) do with lis[larg] do
    begin
      v := min(v,eval(car,car_type));
      larg := cdr;
    end;
  eval_min := v;
end;

function  eval_max(larg : integer) : extended;
var v : extended;
begin
  v := -maxextended;
  while ( larg <> 0 ) do with lis[larg] do
    begin
      v := max(v,eval(car,car_type));
      larg := cdr;
    end;
  eval_max := v;
end;

function  eval_stepf(larg : integer) : extended;
var v,a,b : extended;
begin
  eval_stepf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type);
      larg := cdr;
    end;
  with lis[larg] do
    begin
      a := eval(car,car_type);
      larg := cdr;
    end;
  with lis[larg] do b := eval(car,car_type);
  if ( v >= a ) and ( v <= b ) then
    eval_stepf := 1.0
  else
    eval_stepf := 0.0;
end;

function  eval_gaussf(larg : integer) : extended;
var v,w : extended;
begin
  eval_gaussf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type);
      larg := cdr;
    end;
  with lis[larg] do w := eval(car,car_type);
  if ( w >= 0.0 ) then
    eval_gaussf := gauss(v,w)
  else
    erreur_eval('function argument: gaussf(' + s_ecri_val(v) + ')');
end;

function  eval_lognormf(larg : integer) : extended;
var v,w : extended;
begin
  eval_lognormf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v > 0.0 ) and ( w > 0 ) then
    eval_lognormf := lognorm(v,w)
  else
    erreur_eval('function argument: lognorm(' + s_ecri_val(v) + ')');
end;

function  eval_binomf(larg : integer) : extended;
var v,w : extended;
begin
  eval_binomf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v >= 0.0 ) and ( w >= 0.0 ) and ( w <= 1.0 ) then
    eval_binomf := binomf(round(v),w)
  else
    erreur_eval('function argument: binomf(' + s_ecri_val(v) + ',' + s_ecri_val(w) + ')');
end;

function  eval_poissonf(larg : integer) : extended; 
var v,w : extended; 
begin 
  eval_poissonf := 0.0; 
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v >= 0.0 ) and ( w >= 0.0 ) then 
    eval_poissonf := poissonf(round(v),w) 
  else 
    erreur_eval('function argument: poissonf(' + s_ecri_val(v) + ',' + s_ecri_val(w) + ')');
end; 

function  eval_nbinomf(larg : integer) : extended;
var v,w : extended;
begin
  eval_nbinomf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v > 0.0 ) and ( w > 0.0 ) and ( w <= 1.0 ) then
    eval_nbinomf := nbinomf(v,w)
  else
    erreur_eval('function argument: nbinomf(' + s_ecri_val(v) + ',' + s_ecri_val(w) + ')');
end;

function  eval_nbinom1f(larg : integer) : extended;
var v,w : extended;
begin
  eval_nbinom1f := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type);
  if ( v > 0.0 ) and ( v < w*w ) then
    eval_nbinom1f := nbinom1f(v,w)
  else
    erreur_eval('function argument: nbinom1f(' + s_ecri_val(v) + ',' + s_ecri_val(w) + ')');
end;

function  eval_betaf(larg : integer) : extended;
var v,w : extended;
begin
  eval_betaf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v > 0.0 ) and ( w > 0.0 ) then
    eval_betaf := betaf(v,w)
  else
    erreur_eval('function argument: betaf(' + s_ecri_val(v) + ',' + s_ecri_val(w) + ')');
end;

function  eval_beta1f(larg : integer) : extended;
var v,w : extended;
begin
  eval_beta1f := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v > 0.0 ) and ( w > 0.0 ) and ( v < 1.0 ) and ( w*w < v*(1.0-v) ) then
    eval_beta1f := beta1f(v,w)
  else
    erreur_eval('function argument: beta1f(' + s_ecri_val(v) + ',' +s_ecri_val(w) + ')');
end;

function  eval_bicof(larg : integer) : extended;
var v,w : extended;
begin
  eval_bicof := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v >= 0.0 ) and ( w >= 0.0 ) then
    eval_bicof := bicof(round(v),round(w))
  else
    erreur_eval('function argument: bicof(' + s_ecri_val(v) + ',' + s_ecri_val(w) + ')');
end;

function  eval_tabf(larg : integer) : extended;
var nb_arg : integer;
    v : extended;
    tab : rlarg_type;
begin
  eval_tabf := 0.0;
  nb_arg := 0;
  while ( larg <> 0 ) do with lis[larg] do
    begin
      v := eval(car,car_type);
      if ( v >= 0.0 ) and ( v <= 1.0 ) then
        begin
          nb_arg := nb_arg + 1;
          tab[nb_arg] := v;
        end
      else
        erreur_eval('function argument: tabf ' + s_ecri_val(v));
      larg := cdr;
    end;
  eval_tabf := tabf(tab,nb_arg);
end;

function  eval_bdf(larg : integer) : extended;
var v,b,d,delta : extended;
begin
  eval_bdf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do
    begin
      b := eval(car,car_type);
      larg := cdr;
    end;
  with lis[larg] do
    begin
      d := eval(car,car_type);
      larg := cdr;
    end;
  with lis[larg] do delta := eval(car,car_type);
  if ( v >= 0.0 ) and ( b >= 0.0 ) and ( d >= 0.0 ) and ( delta >= 0.0 ) then
    eval_bdf := bdf(round(v),b,d,delta)
  else 
    erreur_eval('function argument: bdf(' + s_ecri_val(v) + ',' +
                 s_ecri_val(b) + ',' + s_ecri_val(d) + ',' + s_ecri_val(delta) + ')');
end;

function  eval_gratef(larg : integer) : extended;
begin
  with lis[larg] do
    if ( t__ > 0 ) then
      eval_gratef := exp((ln0(variable[car].val) - ln0(variable[car].val0))/t__)
    else
      eval_gratef := 0.0;
end;

function  eval_lambdaf(larg : integer) : extended;
var x,y : integer;
    v,w   : extended;
    lambda : cvec_type;
begin
  eval_lambdaf := 0.0;
  with lis[larg] do v := eval(car,car_type);
  larg := lis[larg].cdr;
  x := trunc(v);
  if ( x <= 0 ) or ( x > modele_nb ) then
    begin
      erreur_eval('function argument: lambdaf ' + s_ecri_val(v));
      exit;
    end;
  if ( modele[x].xmat = 0 ) then
    begin
      erreur_eval('function argument: lambdafnon matrix-type model ');
      exit;
    end;
  with lis[larg] do w := eval(car,car_type);
  y := trunc(w);
  if ( y <= 0 ) or ( y > modele[x].size ) then
    begin
      erreur_eval('function argument: lambdaf ' + s_ecri_val(w));
      exit;
    end;
  with modele[x] do matvalprop(size,mat[xmat].val,lambda);
  if err_math then
    begin
      err_math := false;
      exit;
    end;
  eval_lambdaf := cmxmod(lambda[y]);
end;

function  eval_prevf(larg : integer) : extended;
var z,x,t1 : integer;
begin
  eval_prevf := 0.0;
  x := lis[larg].car;
  larg := lis[larg].cdr;
  with lis[larg] do t1 := trunc(eval(car,car_type));
  if ( t1 < 0 ) or ( t1 > maxprev ) then
    begin
      erreur_eval('function argument: prevf ' + s_ecri_val(t1));
      exit;
    end;
  with variable[x] do
    if ( t1 > t__ ) then
      eval_prevf := val0
    else
      begin
        z := (pprev + maxprev1 - t1) mod maxprev1;
        eval_prevf := prev[z];
      end;
end;

function  eval_textf(larg : integer) : extended;
begin
  eval_textf := variable[lis[larg].car].te;
end;

function  eval_meanf(larg : integer) : extended;
begin
  eval_meanf := variable[lis[larg].car].moy;
end;

function  eval_variancef(larg : integer) : extended;
begin
  eval_variancef := variable[lis[larg].car].variance;
end;

function  eval_skewnessf(larg : integer) : extended;
begin
  eval_skewnessf := variable[lis[larg].car].skewness;
end;

function  eval_cvf(larg : integer) : extended;
begin
  with variable[lis[larg].car] do
    begin
      if ( moy <> 0.0 ) then
        eval_cvf := sqrt(variance)/moy
      else
        eval_cvf := 0.0;
    end;
end;

function  eval_meanzf(larg : integer) : extended;
begin
  eval_meanzf := variable[lis[larg].car].moyz;
end;

function  eval_variancezf(larg : integer) : extended;
begin
  eval_variancezf := variable[lis[larg].car].variancez;
end;

function  eval_cvzf(larg : integer) : extended;
begin
  with variable[lis[larg].car] do
    begin
      if ( moyz <> 0.0 ) then
        eval_cvzf := sqrt(variancez)/moyz
      else
        eval_cvzf := 0.0;
    end;
end;

function  eval_nzf(larg : integer) : extended;
begin
  eval_nzf := variable[lis[larg].car].nz;
end;

function  eval_nef(larg : integer) : extended;
begin
  eval_nef := variable[lis[larg].car].ne;
end;

function  eval_nif(larg : integer) : extended;
begin
  eval_nif := variable[lis[larg].car].ni;
end;

function  eval_extratef(larg : integer) : extended;
begin
  eval_extratef := variable[lis[larg].car].er;
end;

function  eval_immratef(larg : integer) : extended;
begin
  eval_immratef := variable[lis[larg].car].ir;
end;

function  eval_fun1(x,larg : integer) : extended;
var i : integer;
    val_sav : rlarg_type;
begin
  eval_fun1 := 0.0;
  with fun[x] do
    begin
      for i := 1 to nb_arg do with arg[xarg[i]] do
        begin
          if ( larg = 0 ) then
            begin
              erreur_eval('missing arguments in function ' + s_ecri_fun(x));
              exit;
            end;
          val_sav[i] := val; 
          with lis[larg] do 
            begin             
              val := eval(car,car_type);
              larg := cdr;
            end;
        end;
      if ( larg <> 0 ) then
        begin
          erreur_eval('too many arguments in function ' + s_ecri_fun(x));
          exit;
        end;
      eval_fun1 := eval(exp,exp_type);
      for i := 1 to nb_arg do with arg[xarg[i]] do val := val_sav[i];
    end;
end;

function  eval_fun(x,larg : integer) : extended;
begin
  if ( x > fun_nb_predef ) then
    eval_fun := eval_fun1(x,larg) 
  else
  if ( x = fun_if ) then
    eval_fun := eval_if(larg)
  else
  if ( x = fun_min ) then
    eval_fun := eval_min(larg)
  else
  if ( x = fun_max ) then
    eval_fun := eval_max(larg)
  else
  if ( x = fun_stepf ) then
    eval_fun := eval_stepf(larg)
  else
  if ( x = fun_gaussf ) then
    eval_fun := eval_gaussf(larg)
  else
  if ( x = fun_lognormf ) then
    eval_fun := eval_lognormf(larg)
  else
  if ( x = fun_binomf ) then
    eval_fun := eval_binomf(larg)
  else 
  if ( x = fun_poissonf ) then 
    eval_fun := eval_poissonf(larg) 
  else
  if ( x = fun_nbinomf ) then
    eval_fun := eval_nbinomf(larg)
  else
  if ( x = fun_nbinom1f ) then
    eval_fun := eval_nbinom1f(larg)
  else
  if ( x = fun_betaf ) then
    eval_fun := eval_betaf(larg)
  else
  if ( x = fun_beta1f ) then
    eval_fun := eval_beta1f(larg)
  else
  if ( x = fun_bicof ) then
    eval_fun := eval_bicof(larg)
  else
  if ( x = fun_tabf ) then
    eval_fun := eval_tabf(larg)
  else
  if ( x = fun_gratef ) then
    eval_fun := eval_gratef(larg)
  else
  if ( x = fun_bdf ) then
    eval_fun := eval_bdf(larg)
  else
  if ( x = fun_lambdaf ) then
    eval_fun := eval_lambdaf(larg)
  else
  if ( x = fun_prevf ) then
    eval_fun := eval_prevf(larg)
  else
  if ( x = fun_textf ) then
    eval_fun := eval_textf(larg)
  else
  if ( x = fun_meanf ) then
    eval_fun := eval_meanf(larg)
  else
  if ( x = fun_variancef ) then
    eval_fun := eval_variancef(larg)
  else
  if ( x = fun_skewnessf ) then
    eval_fun := eval_skewnessf(larg)
  else
  if ( x = fun_cvf ) then
    eval_fun := eval_cvf(larg)
  else
  if ( x = fun_meanzf ) then
    eval_fun := eval_meanzf(larg)
  else
  if ( x = fun_variancezf ) then
    eval_fun := eval_variancezf(larg)
  else
  if ( x = fun_cvzf ) then
    eval_fun := eval_cvzf(larg)
  else
  if ( x = fun_nzf ) then
    eval_fun := eval_nzf(larg)
  else
  if ( x = fun_nef ) then
    eval_fun := eval_nef(larg)
  else
  if ( x = fun_nif ) then
    eval_fun := eval_nif(larg)
  else
  if ( x = fun_extratef ) then
    eval_fun := eval_extratef(larg)
  else
  if ( x = fun_immratef ) then
    eval_fun := eval_immratef(larg);
end;

function eval_convol(x,tx,y,ty : integer) : extended;
var i,n : integer;
    s : extended;
begin
  eval_convol := 0.0;
  s := eval(y,ty);
  if ( s > bigint ) then
    begin
      erreur_eval('second operand too big in operator @');
      exit;
    end;
  n := trunc(s);
  s := 0.0;
  if ( tx = type_variable ) then with variable[x] do
    for i := 1 to n do s := s + eval(exp,exp_type)
  else
    for i := 1 to n do s := s + eval(x,tx);
  eval_convol := trunc(s); 
end;


{ ------ eval ------ }

function  eval_lis(x : integer) : extended;
var op,top,z : integer;
    v,w : extended;
begin
  eval_lis := 0.0;
  if ( x = 0 ) then
    begin
      erreur_eval('empty list');
      exit;
    end;
  with lis[x] do
    begin
      op  := car;
      top := car_type;
      z   := cdr;
    end;
  case top of
  type_op1 : begin
               with lis[z] do v := eval(car,car_type);
               eval_lis := eval_op1(op,v);
             end;
  type_op2 : begin
               if ( op = op2_convol ) then 
                 begin
                   with lis[z] do
                     eval_lis := eval_convol(car,car_type,
                                 lis[cdr].car,lis[cdr].car_type);
                    exit;
                 end;
               with lis[z] do
                 begin
                   v := eval(car,car_type);
                   z := cdr;
                 end;
               with lis[z] do w := eval(car,car_type);
               eval_lis := eval_op2(op,v,w);
             end;
  type_fun : begin
               eval_lis := eval_fun(op,z);
             end;
  else writeln('eval_lis?');
  end;
end;

function  eval(x,tx : integer) : extended;
begin
  eval := 0.0;
  case tx of
    type_lis       : eval := eval_lis(x);
    type_variable  : eval := variable[x].val;
    type_arg       : eval := arg[x].val;
    type_ree       : eval := ree[x].val;
    else writeln('eval?');
  end;
end;

function  occur_var(x,tx,y : integer) : boolean;
{ regarde si la variable y a une occurence dans l'expression x  }
begin
 occur_var := false;
 case tx of
    type_lis : 
      while( x <> 0 ) do with lis[x] do
        begin
          if ( occur_var(car,car_type,y) ) then
            begin
              occur_var := true;
              exit;
            end;
           x := cdr;
        end;
    type_variable :
      occur_var := y = x;
    type_fun : 
      with fun[x] do occur_var := occur_var(exp,exp_type,y);
    else;
  end;
end;

function  occur_fun1(x,f : integer) : integer;
{ regarde si la fonction f intervient dans la liste x }
{ retourne la liste des arguments de f }
var z : integer;
begin
  while ( x <> 0 ) do with lis[x] do
    begin
      if ( car_type = type_fun ) and ( car = f ) then
        begin
          occur_fun1 := cdr;
          exit;
        end
      else
        if ( car_type = type_lis ) then
          begin
            z := occur_fun1(car,f);
            if ( z <> 0 ) then
              begin
                occur_fun1 := z;
                exit;
              end;
          end;
      x := cdr;
    end;
  occur_fun1 := 0;
end;

procedure pre_eval1(exp : integer);
{ on ne considere que la premiere occurence dans chaque variable ... }
var z : integer;
begin
  z := occur_fun1(exp,fun_gratef);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function gratef');
  z := occur_fun1(exp,fun_prevf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function prevf');
  z := occur_fun1(exp,fun_textf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function textf');
  z := occur_fun1(exp,fun_meanf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function meanf');
  z := occur_fun1(exp,fun_variancef);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function variancef');
  z := occur_fun1(exp,fun_skewnessf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function skewnessf');
  z := occur_fun1(exp,fun_cvf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function cvf');
  z := occur_fun1(exp,fun_meanzf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function meanzf');
  z := occur_fun1(exp,fun_variancezf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function variancezf');
  z := occur_fun1(exp,fun_cvzf);
    if ( z <> 0 ) then with lis[z] do
      if ( car_type <> type_variable ) then
        erreur_eval('variable name expected in function cvzf');
  z := occur_fun1(exp,fun_extratef);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function extratef');
  z := occur_fun1(exp,fun_immratef);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function immratef');
end;

procedure pre_eval;
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type = type_lis ) then pre_eval1(exp);
  for x := 1 to rel_nb do with rel[x] do
    if ( exp_type = type_lis ) then pre_eval1(exp);
end;

{ ------ init_eval ------ }

procedure init_var(x : integer);
var i : integer;
begin
  with variable[x] do
    begin
      te  := 0;
      for i := 0 to maxprev do prev[i] := val; {jjjjj}
      sum  := val;
      sum2 := val*val;
      sum3 := val*val*val;
      moy  := val;
      variance  := 0.0;
      skewness  := 0.0;
      variancez := 0.0;
      if ( val < seuil_ext ) then
        begin
          nz := 1;
          moyz  := 0.0;
          sumz  := 0.0;
          sum2z := 0.0;
        end
      else
        begin
          nz := 0;
          moyz  := val;
          sumz  := val;
          sum2z := val*val;
        end;
      ne := 0;
      ni := 0;
      er := 0.0;
      ir := 0.0;
    end;
end;

procedure eval_variable_00;
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    begin
      if ( exp_type = type_inconnu ) then
        erreur_eval('variable ' + s_ecri_var(x) + ' not initialized');
      if ( exp_type = type_ree ) then
        begin
          val0 := ree[exp].val;
          val  := val0;
          init_var(x);
        end;
    end;
end;

procedure eval_rel_00;
var x : integer;
begin
  for x := 1 to rel_nb do with rel[x] do val0 := variable[xvar].val0;
end;

procedure eval_vec_00;
var x,i : integer;
begin
  for x := 1 to vec_nb do with vec[x] do
    for i := 1 to size do
      val0[i] := variable[exp[i]].val0;
end;

procedure eval_modele_00;
var x,i : integer;
    a : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    if ( xmat <> 0 ) then with vec[xvec] do
      begin
        a := 0.0;
        for i := 1 to size do a := a + val0[i];
        pop0 := a;
      end
    else
      begin
        a := 0.0;
        for i := 1 to size do a := a + rel[xrel[i]].val0;
        pop0 := a;
      end;
end;

procedure graph_variable;
var  x,y,e,niv,nivmax1 : integer;
     tab1,tab2 : array[1..variable_nb_max] of integer;

procedure parc0(x : integer);
var i,j,y : integer;
begin
  if ( tab1[x] = 1 ) then exit;
  niv := niv + 1;
  tab1[x] := 1;
  tab2[niv] := x;
  with ggg[x] do
    for i := 1 to succ_nb do
      begin
        y := succ[i];
        for j := 1 to niv do
          if ( tab2[j] = y ) then
            begin
              erreur_eval('cycling definition of variable ' + s_ecri_var(x));
              exit;
            end;
        parc0(y);
      end;
  niv := niv - 1;
end;

procedure parc1(x : integer);
var i,y : integer;
begin
  niv := niv + 1;
  if ( niv > nivmax1 ) then nivmax1 := niv;
  tab1[x] := niv;
  with ggg[x] do
    for i := 1 to succ_nb do
      begin
        y := succ[i];
        if ( tab1[y] < niv ) then parc1(y);
      end;
  niv := niv - 1;
end;

begin

{ graphe des variables }

  for x := 1 to variable_nb do with ggg[x] do succ_nb := 0;
  for x := 1 to variable_nb do with variable[x] do
    for y := 1 to variable_nb do
      if occur_var(exp,exp_type,y) then
        with ggg[x] do
          begin
            succ_nb := succ_nb + 1;
            if (succ_nb > succ_nb_max) then
              begin
                erreur_eval('graph of variables too large');
                halt;
              end;
            succ[succ_nb] := y;
          end;

  {for x := 1 to variable_nb do with ggg[x] do
    begin
      iwriteln('* ' + s_ecri_var(x));
      for y := 1 to succ_nb do iwriteln('   ' + s_ecri_var(succ[y]));
    end;}

{ 1. detection des cycles }
  for x := 1 to variable_nb do tab1[x] := 0;
  for x := 1 to variable_nb do
    begin
      niv := 0;
      parc0(x);
      if err_eval then exit;
    end;

{ 2. evaluer les variables selon leur hierarchie }
{ les variables des relations sont reelles, donc evaluees avant }

  nivmax1 := 0;
  for x := 1 to variable_nb do tab1[x] := 0;
  for x := 1 to variable_nb do
    begin
      niv := tab1[x];
      parc1(x);
    end;

  {for niv := nivmax1 downto 1 do
    for x := 1 to variable_nb do
      if ( tab1[x] = niv ) then
        iwriteln(s_ecri_val(niv) + ' -> ' + s_ecri_var(x)); }

  e := 0;
  for niv := nivmax1 downto 1 do
    for x := 1 to variable_nb do with variable[x] do
      if ( exp_type <> type_ree ) then
        if ( tab1[x] = niv ) then
          begin
            e := e + 1;
            ordre_eval[e] := x;
          end;
  ordre_eval_nb := e;
end;

procedure eval_variable_0;
var e,x : integer;
begin
  for e := 1 to ordre_eval_nb do
    begin
       x := ordre_eval[e];
       with variable[x] do
         begin
           val0 := eval(exp,exp_type);
           val  := val0;
           init_var(x);
         end;
    end;
end;

procedure eval_variable_1;
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type = type_ree ) then
      begin
        val := val0;
        init_var(x);
      end;
  eval_variable_0;
end;

procedure eval_rel_0;
var x : integer;
begin
  for x := 1 to rel_nb do with rel[x] do val := val0;
end;

procedure eval_vec_0;
var x,i : integer;
begin
  for x := 1 to vec_nb do with vec[x] do
    for i := 1 to size do
      val[i] := val0[i];
end;

procedure eval_mat_0;
var x,i,j : integer;
begin
  for x := 1 to mat_nb do with mat[x] do
    for i := 1 to size do
      for j := 1 to size do
        begin
          val0[i,j] := eval(exp[i,j],exp_type[i,j]);
          val[i,j]  := val0[i,j];
        end;
end;

procedure eval_mat_1;
var x,i,j : integer;
begin
  for x := 1 to mat_nb do with mat[x] do
    for i := 1 to size do
      for j := 1 to size do
        val[i,j] := val0[i,j];
end;

procedure eval_modele_0;
var x : integer;
begin
  for x := 1 to modele_nb do with modele[x] do pop := pop0;
end;

procedure init_eval;
begin 
  init_alea;
  err_eval := false;
  pre_eval;
  if err_eval then err_eval := false;
  with variable[xtime] do val := 0.0;
  t__ := 0;
  pprev := 0;
  eval_variable_00;
  eval_rel_00;
  eval_vec_00;
  eval_modele_00;
  eval_rel_0;
  eval_vec_0;
  eval_modele_0;
  graph_variable;
  eval_variable_0;
  eval_mat_0;
end;

procedure init_eval1;
begin
  init_alea;
  variable[xtime].val := 0.0;
  t__ := 0;
  pprev := 0;
  eval_rel_0;
  eval_vec_0;
  eval_modele_0;
  eval_variable_1;
  eval_mat_1;
end;

{ ------ run_t ------ }

procedure eval_mat(x : integer);
var i,j : integer;
begin
  with mat[x] do
    for i := 1 to size do
      for j := 1 to size do
        val[i,j] := eval(exp[i,j],exp_type[i,j])
end;

procedure run_modele;
var x,i : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    if ( xmat <> 0 ) then with vec[xvec] do
      begin
        eval_mat(xmat);
        matvec(size,mat[xmat].val,val,val);
      end
    else
      for i := 1 to size do with rel[xrel[i]] do val := eval(exp,exp_type);
end;

procedure run_rel;
var x : integer;
begin
  for x := 1 to rel_nb do with rel[x] do 
    if ( xmodele = 0 ) then val := eval(exp,exp_type);
end;

procedure maj_modele;
var x,i : integer;
    a : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    if ( xmat <> 0 ) then with vec[xvec] do
      begin
        a := 0.0;
        for i := 1 to size do
          begin
            variable[exp[i]].val := val[i];
            a := a + val[i];
          end;
        pop  := a;
      end
    else
      begin
        a := 0.0;
        for i := 1 to size do with rel[xrel[i]] do
          begin
            variable[xvar].val := val;
            a := a + val;
          end;
        pop := a;
      end;
end;

procedure maj_rel;
var x : integer;
begin
  for x := 1 to rel_nb do with rel[x] do
    if ( xmodele = 0 ) then
      variable[xvar].val := val;
end;

procedure maj_variable;
var e,x,pprev1 : integer;

procedure maj_var(x : integer);
var val1 : extended;
begin
  with variable[x] do
    begin
      prev[pprev] := val;
      val1 := prev[pprev1];
      if ( te = 0 ) then if ( val < seuil_ext) then te := t__;
      if ( val < seuil_ext ) then
          nz := nz + 1
      else
        begin
          sumz  := sumz  + val;
          sum2z := sum2z + val*val;
        end;
      sum  := sum  + val;
      sum2 := sum2 + val*val;
      sum3 := sum3 + val*val*val;
      moy  := sum/(t__+1);
      variance := max(sum2 - moy*sum,0.0)/t__;
      skewness := max(sum3 - moy*(3.0*sum2 - 2.0*moy*sum),0.0);
      if ( variance > 0.0 ) then
        skewness := skewness/(t__*sqrt(variance*variance*variance))
      else
        skewness := 0.0;
      if ( t__+1 > nz ) then
        moyz := sumz/(t__+1 - nz)
      else
        moyz := 0.0;
      if ( t__> nz ) then
        variancez := max(sum2z - sumz*sumz/(t__+1 - nz),0.0)/(t__ - nz)
      else
        variancez := 0.0;
      if ( val < seuil_ext )  and ( val1 >= seuil_ext ) then ne := ne + 1;
      if ( val >= seuil_ext ) and ( val1 < seuil_ext )  then ni := ni + 1;
      if ( val < seuil_ext ) then
        begin
          if ( t__+1 > nz ) then er := ne/(t__+1 - nz) else er := 0.0;
          if ( nz > 1 )     then ir := ni/(nz - 1)     else ir := 0.0;
        end
      else
        begin
          if ( t__ > nz ) then er := ne/(t__ - nz) else er := 0.0;
          if ( nz > 0 )   then ir := ni/nz         else ir := 0.0;
        end;
    end;
end;

procedure maj_var2(x : integer);
{ modif pour tenir compte du temps d'observation tobs }
{ quantites moy, cv, ... calculees a partir de t = tobs }
{ commande "; 1000" -> tobs = 1000 }
{ etude schoener }
var val1 : extended;
    t1 : integer;
begin
  with variable[x] do
    begin
      prev[pprev] := val;
      val1 := prev[pprev1];
      if ( t__ <= tobs ) then
        begin
          init_var(x);
          exit;
        end;
      t1 := t__ - tobs;
      if ( te = 0 ) then if ( val < seuil_ext) then te := t1;
      if ( val < seuil_ext ) then
        nz := nz + 1
      else
        begin
          sumz  := sumz  + val;
          sum2z := sum2z + val*val;
        end;
      sum  := sum  + val;
      sum2 := sum2 + val*val;
      {sum3 := sum3 + val*val*val; }
      moy  := sum/(t1+1);
      variance := max(sum2 - moy*sum,0.0)/t1;
      {skewness := max(sum3 - moy*(3.0*sum2 - 2.0*moy*sum),0.0);
      if ( variance > 0.0 ) then
        skewness := skewness/(t1*sqrt(variance*variance*variance))
      else
        skewness := 0.0;}
      if ( t1+1 > nz ) then
        moyz := sumz/(t1+1 - nz)
      else
        moyz := 0.0;
      if ( t1 > nz ) then
        variancez := max(sum2z - sumz*sumz/(t1+1 - nz),0.0)/(t1 - nz)
      else
        variancez := 0.0;
      if ( val < seuil_ext )  and ( val1 >= seuil_ext ) then ne := ne + 1;
      if ( val >= seuil_ext ) and ( val1 < seuil_ext )  then ni := ni + 1;
      if ( val < seuil_ext ) then
        begin
          if ( t1+1 > nz ) then er := ne/(t1+1 - nz) else er := 0.0;
          if ( nz > 1 )    then ir := ni/(nz - 1)    else ir := 0.0;
        end
      else
        begin
          if ( t1 > nz ) then er := ne/(t1 - nz) else er := 0.0;
          if ( nz > 0 )  then ir := ni/nz        else ir := 0.0;
        end;
    end;
end;

begin
  t__ := t__ + 1;
  with variable[xtime] do val := val + 1.0;
  pprev1 := pprev;
  pprev  := (pprev + 1) mod maxprev1;
  for x := 1 to variable_nb do with variable[x] do
    if ( xrel <> 0 ) or ( xvec <> 0 ) then
      {maj_var(x);}
      maj_var2(x);
  for e := 1 to ordre_eval_nb do
    begin
      x :=  ordre_eval[e];
      with variable[x] do
        begin
          val := eval(exp,exp_type);
          maj_var(x);
        end;
    end;
end;

procedure run_t; 
begin

{ 1.a -  evaluation les relations liees aux modeles : }

  run_modele;

{ 1.b - evaluation les autres relations : }

  run_rel;


{ 2.a - mise a jour les variables des relations liees aux modeles : }

  maj_modele;

{ 2.b - mise a jour les variables des autres relations : }

  maj_rel;

{ 3 - mise a jour des autres variables : }

  maj_variable;

end;

end.
