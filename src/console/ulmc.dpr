{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

program ulmc;

{$APPTYPE CONSOLE}

uses  qglobvar,qsymb,qsyntax,qmath,qcompil,qeval,
      qmatrix,qinterp,qrun,qcarlo;

procedure erreur_ulm(s : string);
begin
  writeln('Error: ',s);
end;

procedure init_defaults;
begin
  nb_cycle := 50;
  dt_texte_interp := 10;
  nb_cycle_carlo := 50;
  nb_run_carlo := 100;
  seuil_ext := seuil_ext_def;
  seuil_div := seuil_div_def;
  param := false;
end;

procedure init_fic_param;
begin
  modelfile  := false;
  inputfile  := false;
  outputfile := false;
  nomficmodele := '';
  nomficin     := '';
  nomficout    := '';
  if ( ParamCount > 0 ) then
    nomficmodele := ParamStr(1)
  else
    begin
      write('File name ? ');
      readln(nomficmodele);
      nomficmodele := tronque(nomficmodele);
      if ( nomficmodele = '' ) then halt;
    end;
  assign(ficmodele,nomficmodele);
{$I-}
  reset(ficmodele);
  if ( IOresult  <> 0 ) then
    begin
      erreur_ulm(nomficmodele +' file not found');
      readln;
      halt;
    end;
{$I+}
  modelfile := true;
  if ( ParamCount > 1 ) then
    begin
      nomficin := ParamStr(2);
      assign(ficin,nomficin);
{$I-}
      reset(ficin);
      if ( IOresult  <> 0 ) then
        begin
          erreur_ulm(nomficin +' file not found');
          readln;
          exit;
        end;
{$I+}
      inputfile := true;
      if ( Paramcount > 2 ) then
        begin
          nomficout := ParamStr(3);
          assign(output,nomficout);
{$I-}
          rewrite(output);
          if ( IOresult <> 0 ) then
            begin
              erreur_ulm(nomficout +' could not open file');
              readln;
              exit;
            end;
{$I+}
          outputfile := true;
        end;
    end;
end;

procedure init_ulm;
begin
  init_math;
  init_symb;
  init_defaults;
  init_fic_param;
end;

procedure fin_ulm;
var i : integer;
begin
  for i := 1 to fic_nb do CloseFile(fic[i].f);
end;

procedure interp1;
var s : string;
begin
  if inputfile then
    if eof(ficin) then
      fini := true
    else
      begin
        readln(ficin,s);
        s := tronque(minuscule(s));
        writeln('> ',s);
        interp(s);
      end
  else
    begin
      write('? ');
      readln(s);
      interp(s);
    end
end;

begin
  writeln('       ULM');
  writeln('Unified Life Models');
  writeln('');
  init_ulm;
  compilation;
  init_eval;
  init_texte_interp;
  fini := false;
  while not fini do interp1;
  fin_ulm;
end.

