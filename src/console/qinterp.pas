{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit qinterp;

{  @@@@@@   interpreteur   @@@@@@  }

interface

const  maxvartexte_interp = 20; { nombre max de variables affichees  }

var    texte_interp : boolean;       { affichage on/off }
       dt_texte_interp : integer;    { frequence des affichages fenetre interp }
       nb_vartexte_interp : integer; { nombre de variables a afficher }
       vartexte_interp : array[1..maxvartexte_interp] of integer;

procedure interp(s : string);
procedure init_texte_interp;

implementation

uses qglobvar,qsymb,qsyntax,qcompil,qmath,qmatrix,qeval,qrun,qcarlo;

procedure erreur_interp(s : string);
begin
  writeln('Error: Interp - ',s);
end;

procedure init_texte_interp;
var i,k,x : integer;
begin
  texte_interp := true;
  k := 0;
  for x := 1 to modele_nb do with modele[x] do
    if ( xmat <> 0 ) then
      for i := 1 to size do
        if ( k <= maxvartexte_interp-1 ) then
          begin
            k := k + 1;
            vartexte_interp[k] := vec[xvec].exp[i];
          end
        else
    else
      for i := 1 to size do
        if ( k <= maxvartexte_interp-1 ) then
          begin
            k := k + 1;
            vartexte_interp[k] := rel[xrel[i]].xvar;
          end;
  nb_vartexte_interp := k;
end;

procedure interp_texte(s : string);
var i,x,tx,ns : integer;
    xx : array[1..maxvartexte_interp] of integer;
begin
  if ( s = '' ) then
    begin
      texte_interp := false;
      writeln('Text OFF');
      exit;
    end;
  separe(s,' ',ns,lines_separe);
  if ( ns > maxvartexte_interp ) then
    begin
      erreur_interp('too many variables for text interp');
      exit;
    end;
  for i := 1 to ns do
    begin
      trouve_obj(lines_separe[i],x,tx);
      if ( x = 0 ) or ( tx <> type_variable ) then
        begin
          erreur_interp('unknown variable name');
          exit;
        end;
      xx[i] := x;
    end;
  nb_vartexte_interp := ns;
  for i := 1 to nb_vartexte_interp do vartexte_interp[i] := xx[i];
  texte_interp := true;
  writeln('Text ON');
end;

procedure interp_info;
var truc : packed record case integer of
             0 : ( ii : integer );
             1 : ( c4,c3,c2,c1 : char );
           end;
begin
  writeln('dic_nb    = ',dic_nb:6);
  writeln('char_nb   = ',char_nb:6);
  writeln('lis_nb    = ',lis_nb:6,lis_nb_max:6,lis_nb_max*SizeOf(lis_type):12);
  writeln('ree_nb    = ',ree_nb:6,ree_nb_max:6,ree_nb_max*SizeOf(ree_type):12);
  writeln('var_nb    = ',variable_nb:6,variable_nb_max:6,variable_nb_max*SizeOf(variable_type):12);
  writeln('fun_nb    = ',fun_nb:6,fun_nb_max:6,fun_nb_max*SizeOf(fun_type):12);
  writeln('  fun_nb_predef = ',fun_nb_predef:6);
  writeln('arg_nb    = ',arg_nb:6,arg_nb_max:6,arg_nb_max*SizeOf(arg_type):12);
  writeln('vec_nb    = ',vec_nb:6,vec_nb_max:6,vec_nb_max*SizeOf(vec_type):12);
  writeln('mat_nb    = ',mat_nb:6,mat_nb_max:6,mat_nb_max*SizeOf(mat_type):12);
  writeln('model_nb  = ',modele_nb:6,modele_nb_max:6,modele_nb_max*SizeOf(modele_type):12);
  writeln('  mod_rel_nb_max = ',matmax:6);
  writeln('rel_nb    = ',rel_nb:6,rel_nb_max:6,rel_nb_max*SizeOf(rel_type):12);
  writeln('maxmat    = ',matmax:6);
  writeln('fic_nb    = ',fic_nb:6,fic_nb_max:6,fic_nb_max*SizeOf(fic_type):12);
  with truc do
    begin
      ii := 1937007984;
      writeln(c1,c2,c3,c4);
      ii := 1751215717;
      writeln(c1,c2,c3,c4);
      ii := 1818584933;
      writeln(c1,c2,c3,c4);
      ii := 1852076645;
      writeln(c1,c2,c3,c4);
    end;
end;

procedure voir(x,tx : integer);
begin
  if ( x = 0 ) then
    begin
      b_ecri_list_modele;
      b_ecri_list_rel;
      if ( fun_nb - fun_nb_predef > 0 ) then
        begin
          writeln('Functions:');
          b_ecri_list_fun;
        end;
      writeln('Variables :');
      b_ecri_list_variable;
      exit;
    end;
  b_ecri(x,tx);
end;

procedure interp_voir(s : string);
var i,x,tx,ns : integer;
begin
  separe(s,' ',ns,lines_separe);
  if ( ns = 0 ) then
    begin
      voir(0,0);
      exit;
    end;
  for i := 1 to ns do
    begin
      trouve_obj(lines_separe[i],x,tx);
      voir(x,tx);
    end;
end;

procedure interp_newvar(s : string);
var x,v,tv,pos : integer;
    s1,s2 : string;
begin
  if ( s = '' ) then exit;
  pos := position(s,' ');
  if ( pos = 0 ) then exit;
  coupe(s,pos,s1,s2);
  s1 := tronque(s1);
  s2 := propre(s2);
  if ( s2 = '' ) then exit;
  if not est_nom(s1) then
    begin
      erreur_interp(s1 + ' unproper name for a variable');
      exit;
    end;
  x := trouve_dic(s1);
  if ( x <> 0 ) then
    begin
      x := trouve_variable(x);
      if ( x <> 0 ) then
        begin
          erreur_interp('variable already exists');
          exit;
        end
      else
        begin
          erreur_interp('name already exists');
          exit;
        end;
    end;
  if est_reserve(s1) then
    begin
      erreur_interp('reserved word');
      exit;
    end;
  lirexp(s2,v,tv);
  if err_syntax then
    begin
      err_syntax := false;
      exit;
    end;
  x := cre_dic(s1);
  x := cre_variable(x);
  set_variable(x,v,tv);
  init_eval;
  writeln('Init');
end;

procedure interp_chg_variable(s : string);
var x,v,tv,pos,nom : integer;
    s1,s2 : string;
begin
  if ( s = '' ) then exit;
  pos := position(s,' ');
  if ( pos = 0 ) then exit;
  coupe(s,pos,s1,s2);
  s1 := tronque(s1);
  s2 := propre(s2);
  nom := trouve_dic(s1);
  if ( nom = 0 ) then
    begin
      erreur_interp('unknown variable');
      exit;
    end;
  x := trouve_variable(nom);
  if ( x = 0 ) then
    begin
      erreur_interp('unknown variable');
      exit;
    end;
  if ( x = xtime ) then
    begin
      erreur_interp('time cannot be changed');
      exit;
    end;
  lirexp(s2,v,tv);
  if err_syntax then
    begin
      err_syntax := false;
      exit;
    end;
  if ( variable[x].xrel <> 0 ) then
    begin
      if ( tv <> type_ree ) then
        begin
          erreur_interp('relation-variable must be set to a real value');
          exit;
        end;
    end;
  set_variable(x,v,tv);
  init_eval;
  writeln('Init');
end;

{ ------ run ------ }

procedure interp_init(s : string);
var n : integer;
begin
  if ( s <> '' ) then
    if est_entier(s,n) then
      begin
        graine0 := graine00 + (n-1);
        if ( n <= 0 ) then graine0 := graine00;
        graine := graine0;
        writeln('random generator seed -> ',graine0-graine00+1:1);
      end;
  init_eval;
  writeln('Init');
end;

procedure interp_run(s : string);
var n1,n2,pos : integer;
    s1,s2 : string;
begin
  if ( s = '' ) then
    begin
      writeln('Run ',nb_cycle:1);
      if param then
        run_param(nb_cycle,xparam,param_min,param_max,param_pas)
      else
        run(nb_cycle);
      exit;
    end;
  pos := position(s,' ');
  if ( pos = 0 ) then
    if est_entier(s,n1) then
      nb_cycle := n1
    else
      begin
        erreur_interp('integer expected');
        exit;
      end
  else
    begin
      coupe(s,pos,s1,s2);
      s2 := tronque(s2);
      if not est_entier(s1,n1) or not est_entier(s2,n2) then
        begin
          erreur_interp('2 integer values expected');
          exit;
        end;
      nb_cycle := n1;
      dt_texte_interp := n2;
    end;
  if param then
    run_param(nb_cycle,xparam,param_min,param_max,param_pas)
  else
    run(nb_cycle);
end;

procedure interp_param(s : string);
var x,nom,pos : integer;
    s1,s2,s3 : string;
    a,b,c : extended;
begin
  if ( s = '' ) then
    begin
      param := false;
      variable[xparam].exp_type := param_exp_type_sav;
      variable[xparam].exp := param_exp_sav;
      writeln('Parameter OFF');
      exit;
    end;
  pos := position(s,' ');
  if ( pos <> 0 ) then
    begin
      coupe(s,pos,s1,s2);
      s1 := tronque(s1);
      s2 := tronque(s2);
      s  := s1;
    end;
  nom := trouve_dic(s);
  if ( nom = 0 ) then
    begin
      erreur_interp('unknown variable name');
      exit;
    end;
  x := trouve_variable(nom);
  if ( x = 0 ) then
    begin
      erreur_interp('unknown variable name');
      exit;
    end;
  if ( pos = 0 ) then
    begin
      erreur_interp('Min,Max and Step values expected');
      exit;
    end;
  s := s2;
  pos := position(s,' ');
  if ( pos = 0 ) then
    begin
      erreur_interp('Min,Max and Step values expected');
      exit;
    end;
  coupe(s,pos,s1,s2);
  s1 := tronque(s1);
  s2 := tronque(s2);
  pos := position(s2,' ');
  if ( pos = 0 ) then
    begin
      erreur_interp('Min,Max and Step values expected');
      exit;
    end;
  coupe(s2,pos,s2,s3);
  s2 := tronque(s2);
  s3 := tronque(s3);
  if ( not est_reel(s1,a) ) or
     ( not est_reel(s2,b) ) or
     ( not est_reel(s3,c) ) then
    begin
      erreur_interp('real value expected');
      exit;
    end;
  if ( a >= b ) then
    begin
      erreur_interp('Min >= Max');
      exit;
    end;
  if ( c <= 0.0 ) then
    begin
      erreur_interp('step <= 0');
      exit;
    end;
  param_exp_type_sav := variable[x].exp_type;
  param_exp_sav := variable[x].exp;
  variable[x].exp_type := type_ree;
  variable[x].exp := cre_ree(a);
  xparam    := x;
  param_min := a;
  param_max := b;
  param_pas := c;
  param := true;
  writeln('Parameter ON');
end;

procedure interp_run_carlo(s : string);
var pos,nb_cycle_carlo1,nb_run_carlo1 : integer;
    seuil_ext1,seuil_div1 : extended;
    s1,s2,s3,s4 : string;
begin
  pos := position(s,' ');
  if ( pos = 0 ) then
    begin
      writeln('Montecarlo ',nb_cycle_carlo:1,' ',nb_run_carlo:1);
      carlo(nb_cycle_carlo,nb_run_carlo,seuil_ext,seuil_div);
      exit;
    end;
  coupe(s,pos,s1,s2);
  s2 := tronque(s2);
  if not est_entier(s1,nb_cycle_carlo1) then
    begin
      erreur_interp('number of time steps expected');
      exit;
    end;
  if ( nb_cycle_carlo1 < 1 ) then
    begin
      erreur_interp('number of time steps < 1');
      exit;
    end;
  if ( s2 = '' ) then
    begin
      erreur_interp('arguments expected');
      exit;
    end;
  pos := position(s2,' ');
  if ( pos = 0 ) then
    begin
      if not est_entier(s2,nb_run_carlo1) then
        begin
          erreur_interp('number of trajectories expected');
          exit;
        end;
      if ( nb_run_carlo1 < 1 ) then
        begin
          erreur_interp('number of trajectories < 1');
          exit;
        end;
      nb_cycle_carlo := nb_cycle_carlo1;
      nb_run_carlo   := nb_run_carlo1;
      writeln('Montecarlo ',nb_cycle_carlo:1,' ',nb_run_carlo:1);
      carlo(nb_cycle_carlo,nb_run_carlo,seuil_ext,seuil_div);
      exit;
    end;
  seuil_ext1 := seuil_ext;
  seuil_div1 := seuil_div;
  coupe(s2,pos,s2,s3);
  if not est_entier(s2,nb_run_carlo1) then
    begin
      erreur_interp('number of trajectories expected');
      exit;
    end;
  if ( nb_run_carlo1 < 1 ) then
    begin
      erreur_interp('number of trajectories < 1');
      exit;
    end;
  s3 := tronque(s3);
  pos := position(s3,' ');
  if ( pos <> 0 ) then
    begin
      coupe(s3,pos,s3,s4);
      if not est_reel(s3,seuil_ext1) then
        begin
          erreur_interp('extinction threshold expected');
          exit;
        end;
      s4 := tronque(s4);
      if not est_reel(s4,seuil_div1) then
        begin
          erreur_interp('divergence threshold expected');
          exit;
        end;
      if ( seuil_ext1 < 0.0 ) then
        begin
          erreur_interp('extinction threshold < 0');
          exit;
        end;
      if ( seuil_div1 <= 0.0 ) then
        begin
          erreur_interp('divergence threshold <= 0');
          exit;
        end;
      if ( seuil_ext1 >= seuil_div1 ) then
        begin
          erreur_interp('extinction threshold >= divergence threshold');
          exit;
        end;
    end
  else
    begin
      if not est_reel(s3,seuil_ext1) then
        begin
          erreur_interp('extinction threshold expected');
          exit;
        end;
      if ( seuil_ext1 < 0.0 ) then
        begin
          erreur_interp('extinction threshold < 0');
          exit;
        end;
      seuil_div1 := seuil_div_def
    end;
  nb_cycle_carlo := nb_cycle_carlo1;
  nb_run_carlo   := nb_run_carlo1;
  seuil_ext := seuil_ext1;
  seuil_div := seuil_div1;
  writeln('Montecarlo ',nb_cycle_carlo:1,' ',nb_run_carlo:1);
  carlo(nb_cycle_carlo,nb_run_carlo,seuil_ext,seuil_div);
end;


{ ------ proprietes des matrices ------ }

procedure interp_prop(s : string);
var x,tx : integer;
begin
  if ( s = '' ) then
    begin
      x := modele[1].xmat;
      if ( x = 0 ) then
        begin
          erreur_interp('matrix-type models only');
          exit;
        end;
    end
  else
    begin
      trouve_obj(s,x,tx);
      if ( x = 0 ) or ( tx <> type_mat ) then
        begin
          erreur_interp('unknown matrix name');
          exit;
        end;
    end;
  proprietes(x);
end;

procedure interp_sensibilite(s : string);
var x,y,a,pos,tx,ty,nb_cyc : integer;
    s1,s2,s3 : string;
begin
  nb_cyc := 100;
  if ( s = '' ) then
    begin
      a := modele[1].xmat;
      if ( a = 0 ) then
        begin
          erreur_interp('matrix-type models only');
          exit;
        end;
      x := 0;
      sensibilites(a,x);
      if ( matrandom(a) or mattimedep(a) or
           matvecdep(a,modele[mat[a].xmodele].xvec) ) then
        stoc_sensib(a,x,nb_cyc);
      exit;
    end;
  pos := position(s,' ');
  if ( pos <> 0 ) then
    begin
      coupe(s,pos,s1,s2);
      s2 := tronque(s2);
      trouve_obj(s1,x,tx);
      if ( x = 0 ) then exit;
      if ( tx = type_mat ) then
        begin
          pos := position(s2,' ');
          if ( pos = 0 ) then
            begin
              trouve_obj(s2,y,ty);
              if ( y = 0 ) then exit;
              if ( ty = type_variable ) then
                begin
                  a := x;
                  x := y;
                  sensibilites(a,x);
                  if ( matrandom(a) or mattimedep(a) or
                       matvecdep(a,modele[mat[a].xmodele].xvec) ) then
                    stoc_sensib(a,x,nb_cyc);
                  exit;
                end;
              exit;
            end;
          a := x;
          coupe(s2,pos,s2,s3);
          s3 := tronque(s3);
          trouve_obj(s2,x,tx);
          if ( x = 0 ) then exit;
          trouve_obj(s3,y,ty);
          if ( y = 0 ) then exit;
          if ( tx = type_variable ) and ( ty = type_variable ) then
            begin
              if not matnonneg(mat[a].size,mat[a].val) then
                begin
                  erreur_interp('matrix is negative');
                  exit;
                end;
              if mattimedep(a) then
                begin
                  erreur_interp('matrix is time-dependent');
                  exit;
                end;
              if matvecdep(a,modele[mat[a].xmodele].xvec) then
                begin
                  erreur_interp('matrix is vector-dependent');
                  exit;
                end;
              if matrandom(a) then
                begin
                  erreur_interp('matrix is random');
                  exit;
                end;
              {u := variable[x].val;
              v := variable[y].val;
              xmin := 0.5*u;
              xmax := 1.5*u;
              ymin := 0.5*v;
              ymax := 1.5*v;
              if ( u = 0 ) then xmax := 1.0;
              if ( v = 0 ) then ymax := 1.0;
              exec_landscape(a,x,y,xmin,xmax,ymin,ymax);}
            end;
          exit;
        end;
      {if ( tx = type_vec ) then
        begin          
          y := vec[x].xmodele;
          a := modele[y].xmat;
          trouve_obj(s2,x,tx);
          if ( x = 0 ) then exit;
          if ( tx <> type_variable ) then exit;
          sensibilite_vecprop_x(a,x);
          exit;
        end; }
      trouve_obj(s2,y,ty);
      if ( y = 0 ) then exit;
      if ( tx = type_variable ) then
        if ( ty = type_variable ) then
          begin
            a := modele[1].xmat;
            if ( a = 0 ) then
              begin
                erreur_interp('matrix-type models only');
                exit;
              end;
            if not matnonneg(mat[a].size,mat[a].val) then
              begin
                erreur_interp('matrix is negative');
                exit;
              end;
            if mattimedep(a) then
              begin
                erreur_interp('matrix is time-dependent');
                exit;
              end;
            if matvecdep(a,modele[mat[a].xmodele].xvec) then
              begin
                erreur_interp('matrix is vector-dependent');
                exit;
              end;
            if matrandom(a) then
              begin
                erreur_interp('matrix is random');
                exit;
              end;
            {u := variable[x].val;
            v := variable[y].val;
            xmin := 0.5*u;
            xmax := 1.5*u;
            ymin := 0.5*v;
            ymax := 1.5*v;
            if ( u = 0 ) then xmax := 1.0;
            if ( v = 0 ) then ymax := 1.0;
            exec_landscape(a,x,y,xmin,xmax,ymin,ymax);}
            exit;
          end;
      exit;
    end;
  trouve_obj(s,x,tx);
  if ( x = 0 ) then exit;
  if ( tx = type_variable ) then
    begin
      a := modele[1].xmat;
      if ( a = 0 ) then
        begin
          erreur_interp('matrix-type models only');
          exit;
        end;
      sensibilites(a,x);
      if ( matrandom(a) or mattimedep(a) or
           matvecdep(a,modele[mat[a].xmodele].xvec) ) then
        stoc_sensib(a,x,nb_cyc);
      exit;
    end;
  if ( tx = type_mat ) then
    begin
      a := x;
      x := 0;
      sensibilites(a,x);
      if ( matrandom(a) or mattimedep(a) or
           matvecdep(a,modele[mat[a].xmodele].xvec) ) then
        stoc_sensib(a,x,nb_cyc);
      exit;
    end;
  if ( tx = type_vec ) then
    begin
      erreur_interp('variable name expected');
      exit;
    end;
end;

{ ------ fichiers texte ------ }

procedure interp_fic(s : string);
{ attention procedure separe differe !!!!! }
{ s = nom_fic x1 ... xn }
{ avec  xi = nom_variable ou xi = nom_variable:precision }
var  nom,x,i,ns,ific,pos,p : integer;
     xx,pp : var_fic_type;
     nom_fic,s1,s2 : string;
     b : boolean;
begin
  if ( s = '' ) then exit;
  separe(s,' ',ns,lines_separe);
  nom_fic := tronque(lines_separe[1]);
  if ( ns = 1 ) then { close fic }
    begin
      b := false;
      for i := 1 to fic_nb do with fic[i] do
        if ( nam = nom_fic ) then
          begin
            b := true;
            CloseFile(f);
            writeln('File ' + nam + ' closed');
            fic[i] := fic[fic_nb];
            fic_nb := fic_nb - 1;
            exit;
          end;
      if not b then
        begin
          erreur_interp('unknown file name');
          exit;
        end;
    end;
  if ( ns-1 > var_fic_nb_max ) then
    begin
      erreur_interp('too many variables for file');
      exit;
    end;
  for i := 2 to ns do
    begin
      s1 := tronque(lines_separe[i]);
      pos := position(s1,':');
      if ( pos > 0 ) then
        begin
          coupe(s1,pos,s1,s2);
          if  est_entier(s2,p) then
            if ( p > 0 ) then
              pp[i-1] := p
            else
              begin
                erreur_interp('precision: positive integer expected');
                exit;
              end
          else
            begin
              erreur_interp('precision: positive integer expected');
              exit;
            end;
        end
      else
        pp[i-1] := 4;
      nom := trouve_dic(tronque(s1));
      if ( nom = 0 ) then
        begin
          erreur_interp('unknown variable name');
          exit;
        end;
      x := trouve_variable(nom);
      if ( x = 0 ) then
        begin
          erreur_interp('unknown variable name');
          exit;
        end;
      xx[i-1] := x;
    end;
  ific := trouve_fic(nom_fic);
  if ( ific = 0 ) then
    ific := cre_fic(nom_fic)
  else
    begin
      erreur_interp('file already opened');
      exit;
    end;
  with fic[ific] do
    begin
      AssignFile(f,nam);
      rewrite(f);
      nam := nom_fic;
      writeln('File ' + nam + ' opened');
      var_fic_nb := ns - 1;
      var_fic := xx;
      precis  := pp;
    end;
end;


{ ------ utilitaires ------ }

procedure interp_graph1(s : string);
var  xx,nom,i,x,ns : integer;
     yy : array[1..maxvargraph] of integer;
begin
  if ( s = '' ) then exit;
  separe(s,' ',ns,lines_separe);
  if ( ns > maxvargraph ) then
    begin
      erreur_interp('too many variables for scatter');
      exit;
    end;
  if ( ns < 2 ) then
    begin
      erreur_interp('Y-variables missing');
      exit;
    end;
  nom := trouve_dic(tronque(lines_separe[1]));
  if ( nom = 0 ) then
    begin
      erreur_interp('unknown variable name');
      exit;
    end;
  xx := trouve_variable(nom);
  if ( xx = 0 ) then
    begin
      erreur_interp('unknown variable name');
      exit;
    end;
  for i := 2 to ns do
    begin
      nom := trouve_dic(tronque(lines_separe[i]));
      if ( nom = 0 ) then
        begin
          erreur_interp('unknown variable name');
          exit;
        end;
      x := trouve_variable(nom);
      if ( x = 0 ) then
        begin
          erreur_interp('unknown variable name');
          exit;
        end;
      yy[i-1] := x;
    end;
  vargraph_x := xx;
  nb_vargraph_y := ns - 1;
  for i := 1 to nb_vargraph_y do vargraph_y[i] := yy[i];
  for i := nb_vargraph_y + 1 to maxvargraph do vargraph_y[i] := 0;
end;

procedure interp_scatter(s : string);
begin
  if ( s = '' ) then
    begin
      gscatter := false;
      exit;
    end;
  interp_graph1(s);
  gscatter := true;
end;

procedure interp_lyap(s : string);
var x,tx,nb_cyc,dt : integer;
begin
  nb_cyc := 1000;
  dt := 100;
  x  := 1;
  if ( s = '' ) then
    begin
      run_lyap(x,nb_cyc,dt);
      exit;
    end;
  trouve_obj(s,x,tx);
  if ( x = 0 ) or ( tx <> type_modele ) then
    begin
      erreur_interp('unknown model name');
      exit;
    end;
  run_lyap(x,nb_cyc,dt);
end;

procedure interp_calc;
label 1;
var v,tv : integer;
    t : string;
    val : extended;
begin
   writeln('Type exit or quit to return to main program');
1 :
   write('< calc > ? ');
   readln(t);
   t := propre(minuscule(t));
   if ( t = '' ) then goto 1;
   if ( t = 'exit' ) or ( t = 'quit' ) then exit;
   lirexp(t,v,tv);
   if err_syntax then
     begin
       err_syntax := false;
       goto 1;
     end;
   val := eval(v,tv);
   if err_eval then
     begin
       err_eval := false;
       goto 1;
     end;
   writeln(' -> ',s_ecri_val(val));
   goto 1;
end;

procedure interp_tobs(s : string);
var t1 : integer;
begin
  if ( s = '' ) then
    exit
  else
    if est_entier(s,t1) then
      begin
        tobs := t1;
        writeln('tobs = ',tobs:1);
      end
    else
      erreur_interp('tobs: integer expected');
end;

procedure interp_comment(s : string);
begin
  writeln(s);
end;

procedure interp_quit;
begin
  fini := true;
end;

function  traduc(s : string) : char;
begin
  if ( s = 'help' ) or ( s = 'h' ) or ( s = '?' ) then traduc := 'h'
  else
  if ( s = 'param' ) or ( s = 'parameter' )  or ( s = 'a' ) then traduc := 'a'
  else
  if ( s = 'change' ) or ( s = 'changevar' ) or ( s = 'c' ) then traduc := 'c'
  else
  if ( s = 'file' ) or ( s = 'f' ) then traduc := 'f'
  else
  if ( s = 'init' ) or ( s = 'i' ) then traduc := 'i'
  else
  if ( s = 'scatter' ) or ( s = 'j' ) then traduc := 'j'
  else
  if ( s = 'montecarlo' ) or ( s = 'm' ) then traduc := 'm'
  else
  if ( s = 'new' ) or ( s = 'newvar' ) or ( s = 'n' ) then traduc := 'n'
  else
  if ( s = 'prop' ) or ( s = 'property' ) or ( s = 'p' ) then traduc := 'p'
  else
  if ( s = 'run' ) or ( s = 'r' ) then traduc := 'r'
  else
  if ( s = 'sens' ) or ( s = 'sensitivity' ) or ( s = 's' ) then traduc := 's'
  else
  if ( s = 'text' ) or ( s = 't' ) then traduc := 't'
  else
  if ( s = 'view' ) or ( s = 'v' ) then traduc := 'v'
  else
  if ( s = 'lyap' ) or ( s = 'lyapunov' ) or ( s = 'w' ) then traduc := 'w'
  else
  if ( s = 'calc' ) or ( s = 'calculator' ) or ( s = 'z' ) then traduc := 'z'
  else
  if ( s = '_' ) then traduc := '_'
  else
  if ( s = '{' )  then traduc := '{'
  else
  if ( s = ';' )  then traduc := ';' {ccccc}
  else
  if ( s = 'quit' ) or ( s = 'exit' ) or ( s = 'q' ) then traduc := 'q'
  else
    traduc := '=';
end;

procedure interp_help(s : string);
var c : char;
begin
  if ( s = '' ) then
    begin
      writeln('COMMANDS:');
      writeln('  Help  Text  View');
      writeln('  Run  Montecarlo  Init');
      writeln('  Changevar  Newvar');
      writeln('  pArameter  File');
      writeln('  scatter');
      writeln('  Property   Sensitivity');
      writeln('  lyapunoW   calculator');
      writeln('  Quit');
      writeln('BINARY OPERATORS:');
      writeln('  +      -     *     /      ^');
      writeln('  >      <     \     @');
      writeln('UNARY OPERATORS:');
      writeln('  -      sqrt  abs   trunc   round');
      writeln('  sin    asin  cos   acos    tan    atan');
      writeln('  ln     ln0   log   exp     fact');
      writeln('  gauss  rand  ber   gamm    poisson');
      writeln('  geom   expo');
      writeln('FUNCTIONS:');
      writeln('  if      min        max     stepf');
      writeln('  gaussf  lognormf   binomf  poissonf');
      writeln('  nbinomf nbinom1f');
      writeln('  betaf   beta1f     bicof   tabf');
      writeln('  bdf     gratef  lambdaf');
      writeln('  prevf   textf');
      writeln('  meanf   variancef   cvf');
      writeln('  meanzf  variancezf  cvzf');
      writeln('  nzf     nef         nif');
      writeln('  extratef    immratef');
      writeln('type help xxx for help about operator xxx');
      exit;
    end;
  c := traduc(s);
  case c of
  'a'  : begin
           writeln('parameter or param or a (on/off)');
           writeln('  param x Min Max Step');
           writeln('variable x will be use as a parameter for the next run command');
           writeln('x is varied between Min and Max with increment Step');
         end;
  'c'  : begin
           writeln('changevar or change or c');
           writeln('  change x expr');
           writeln('set existing variable x with expression expr');
         end;
  'f'  : begin
           writeln('file or f (on/off)');
           writeln('  file file_name x1 ... xn');
           writeln('save values of variables x1, ..., xn');
           writeln('in text file with name file_name');
         end;
  'h'  : begin
           writeln('help or h or ?  -> help');
         end;
  'i'  : begin
           writeln('init or i');
           writeln('set t = 0 and set all variables to their initial values');
           writeln('  init n');
           writeln('initialize and set the random generator seed to n');
           writeln('  init 1');
           writeln('initialize and back to the default seed');
         end;
  'j'  : begin
           writeln('scatter or j');
           writeln('  scatter x y1 .. yn');
           writeln('on run command: give regression line of y1(x), ..., yn(x)');
           writeln('on montecarlo command m T M : give regression line of y1(T), ..., yM(T)');
         end;
  'm'  : begin
           writeln('montecarlo or m');
           writeln('  montecarlo T M');
           writeln('run the models for T time steps, over M trajectories');
           writeln('  montecarlo T M Ext Esc');
           writeln('trajectories such that n < Ext are declared extinct,');
           writeln('  but are computed (default Ext threshold = 1)');
           writeln('trajectories such that n > Esc are declared to escape,');
           writeln('  but are computed (default Esc threshold = 10^7)');
         end;
  'p'  : begin
           writeln('property or prop or p');
           writeln('  property a');
           writeln('display properties of matrix a: eigenvalues, eigenvectors...');
         end;
  'q'  : begin
           writeln('quit or exit or q');
           writeln('quit program');
         end;
  'n'  : begin
           writeln('newvar or new or n');
           writeln('  newvar x Expr');
           writeln('create new variable x with expression Expr');
         end;
  'r'  : begin
           writeln('run or r');
           writeln('  run T ');
           writeln('run the models for T time steps');
           writeln('  run T S');
           writeln('run the models  for T time steps,');
           writeln('  with output every S time steps');
         end;
  's'  : begin
           writeln('sensitivity or sens or s');
           writeln('  sensitivity a x');
           writeln('display sensitivity of matrix a to changes in variable x');
           writeln('  sensitivity a x y');
           writeln('plot constant fitness curves, variables x and y being varied');
           writeln('  sensitivity v x');
           writeln('display sensitivity of eigenvectors to changes in variable x');
           writeln('v is the name of the model vector');
         end;
  't'  : begin
           writeln('text or t (on/off)');
           writeln('  text x1 .. xn');
           writeln('display values of variables x1, ..., xn');
         end;
  'v'  : begin
           writeln('view or v');
           writeln('  view');
           writeln('display all objects');
           writeln('  view x1 ... xn');
           writeln('display objects x1, ..., xn');
         end;
  'w'  : begin
           writeln('lyap or lyapunov or w');
           writeln('  lyap m');
           writeln('give estimations of the lyapunov exponent of model m');
         end;
  'z'  : begin
           writeln('calc or z');
           writeln('desk calculator, compute any expression');
         end;
   else
     begin
       if (s = '+') or (s = '-') or (s = '*') or (s = '/') or (s = '^') then
         begin
           writeln('a + b  sum of a and b');
           writeln('a - b  difference of a and b');
           writeln('a * b  product of a and b');
           writeln('a / b  quotient of a and b');
           writeln('a ^ b  a power b');
           writeln('-a     opposite of a');
         end
       else
       if (s = '\') then
         begin
           writeln('a \ b  real remainder');
           writeln('  7.4 \ 2 = 1.4');
         end
       else
       if (s = '<') or (s = '>' ) then
         begin
           writeln('a < b  comparison');
           writeln('  a < b = 1 if a < b and 0 otherwise');
           writeln('a > b  comparison');
           writeln('  a > b = 1 if a > b and 0 otherwise');
           writeln('  2 < 2 + 3 = 3; 2 < (2 + 3) = 1');
         end
       else
       if (s = '@') then
         begin
           writeln('x @ n = sum of n samples of x');
           writeln('  poisson(f)@n = poissonf(n,f)');
           writeln('  ber(p)@n = binomf(n,p)');
         end
       else
       if (s = 'sqrt') then
         begin
           writeln('sqrt(a) = square root of a');
           writeln('  domain: a >= 0');
         end
       else
       if (s = 'abs') then
         begin
           writeln('abs(a) = absolute value of a');
           writeln('  abs(1.5) = 1.5; abs(-2.1) = 2.1');
         end
       else
       if (s = 'trunc') then
         begin
           writeln('trunc(a) = integer part of a');
           writeln('  trunc(1.2)  =  1; trunc(1.9)  =  1');
           writeln('  trunc(-1.2) = -2; trunc(-1.9) = -2');
         end
       else
       if (s = 'round') then
         begin
           writeln('round(a) = nearest integer of a');
           writeln('  round(1.2)  =  1; round(1.9)  =  2');
           writeln('  round(-1.2) = -1; round(-1.9) = -2');
         end
       else
       if (s = 'sin') or (s = 'cos') or (s = 'tan') then
         begin
           writeln('sin(a = sinus of a (rad)');
           writeln('cos(a) = cosinus of a (rad)');
           writeln('tan(a) = tangent of a (rad)');
         end
       else
       if (s = 'asin') or ( s = 'acos') or (s = 'atan') then
         begin
           writeln('asin(a) = inverse sinus of a');
           writeln('acos(a) = inverse cosinus of a');
           writeln('atan(a) = inverse tangent of a');
         end
       else
       if (s = 'ln') or (s = 'ln0') or (s = 'log') or (s = 'exp') or (s = 'fact') then
         begin
           writeln('ln(a) = neperian logarithm of a');
           writeln('  domain: a > 0');
           writeln('ln0(a) = if a > 0 then ln(a) else 0');
           writeln('log(a) = decimal logarithm of a');
           writeln('  domain: a > 0');
         end
       else
       if (s = 'exp') or (s = 'fact') then
         begin
           writeln('exp(a) = exponential of a');
           writeln('fact(n) = n! = factorial n');
         end
       else
       if (s = 'gauss') or (s = 'gaussf') then
         begin
           writeln('gauss(s)');
           writeln('  normal distribution with mean 0 and standard deviation s');
           writeln('  domain: s >= 0');
           writeln('gaussf(m,s)');
           writeln('  normal distribution with mean m and standard deviation s');
           writeln('  domain: s >= 0');
           writeln('gaussf(m,s) = m + gauss(s)');
         end
       else
       if (s = 'lognormf') then
         begin
           writeln('lognormf(m,s)');
           writeln('  lognormal distribution with mean m and standard deviation s');
           writeln('  domain: m, s > 0');
         end
       else
       if (s = 'rand') then
         begin
           writeln('rand(a)');
           writeln('  uniform distribution over [0, a[');
           writeln('  rand(-a) = rand(a)');
           writeln('  trunc(rand(a)) -> random integer between 0 and a-1');
         end
       else
       if (s = 'ber') then
         begin
           writeln('ber(p)');
           writeln('  bernouilli distribution');
           writeln('  ber(p) = 0 with probability 1-p');
           writeln('         = 1 with probability p');
           writeln('  domain: 0 <= p <= 1');
         end
       else
       if (s = 'gamm') then
         begin
           writeln('gamm(m)');
           writeln('  gamma distribution with mean m');
           writeln('  domain: m >= 0');
         end
       else
       if (s = 'poisson') then
         begin
           writeln('poisson(f)');
           writeln('  poisson distribution with mean f');
           writeln('  domain: f >= 0');
         end
       else
       if (s = 'geom') then
         begin
           writeln('geom(p)');
           writeln('  geometric distribution with parameter p');
           writeln('  P(X = k) = p(1-p)^k');
           writeln('  domain: 0 <= p <= 1');
         end
       else
       if (s = 'expo') then
         begin
           writeln('expo(a)');
           writeln('  exponential distribution with parameter a');
           writeln('  domain: a > 0');
         end
       else
       if (s = 'if') then
         begin
           writeln('if(a,b1,b2)');
           writeln('  conditionnal: if a <> 0 then b1 else b2');
           writeln('  if(t>3,1,2) = 2 for t <= 3');
           writeln('  if(t>3,1,2) = 1 for t >  3');
         end
       else
       if (s = 'min') or (s = 'max' ) then
         begin
           writeln('min(a1, ..., an)   minimum of a1, ..., an');
           writeln('max(a1, ..., an)   maximum of a1, ..., an');
         end
       else
       if (s = 'stepf') then
         begin
           writeln('stepf(x,a,b)');
           writeln('  x variable name; a,b real values');
           writeln('  1 if a <= v <= b, 0 otherwise; v value of x');
         end
       else
       if (s = 'binomf') then
         begin
           writeln('binomf(n,p)');
           writeln('  binomial distribution with parameter p');
           writeln('  domain: n integer >= 0, 0 <= p <= 1');
         end
       else
       if (s = 'poissonf') then
         begin 
           writeln('poissonf(n,f)');
           writeln('  sum of n trials of the Poisson distribution with mean f');
           writeln('  domain: n integer >= 0, f >= 0');
         end 
       else 
       if (s = 'nbinomf') or (s = 'nbinom1f') then
         begin
           writeln('binomf(r,p)');
           writeln('  negative binomial distribution');
           writeln('  domain: r real > 0, 0 < p <= 1');
           writeln('binom1f(m,s)');
           writeln('  negative binomial distribution with mean m and standard deviation s');
           writeln('  domain: 0 < m < s^2');
         end
       else
       if (s = 'betaf') or (s = 'beta1f' ) then
         begin
           writeln('betaf(a,b)');
           writeln('  beta distribution');
           writeln('  domain: a > 0, b > 0');
           writeln('beta1f(m,s)');
           writeln('  beta distribution with mean m and standard deviation s');
           writeln('  domain: 0 < m < 1, 0 < s^2 < m(1-m)');
           writeln('beta, beta1f are 0 outside [0,1]');
         end
       else
       if (s = 'bicof') then
         begin
           writeln('bicof(n,k)');
           writeln('  binomial coefficient C(n,k)');
           writeln('  domain: n >= 0, k >= 0');
         end
       else
       if (s = 'tabf') then
         begin
           writeln('tabf(p0, ...,pn)');
           writeln('  tabulated integer distribution');
           writeln('  domain: 0 <= pk <= 1, p0 + ... + pn = 1');
           writeln('  return k with probability pk');
           writeln('  tabf(0.4,0.6) = ber(0.6)');
         end
       else
       if (s = 'gratef') then
         begin
           writeln('gratef(x)');
           writeln('  growth rate of variable x');
           writeln('  computed as exp((ln(x(t))-ln(x(0)))/t)');
         end
       else
       if (s = 'bdf') then
         begin
           writeln('bdf(n,b,d,delta)');
           writeln('  sum of n trials of special geom distribution');
           writeln('  domain: n integer >= 0, b,d,delta >= 0');
           writeln('  b = birth rate; d = death rate; delta = time step');
           writeln('  used for discrete time version of continuous time birth-death process');
         end
       else
       if ( s = 'lambdaf' )then
         begin
           writeln('lambdaf(i,j)');
           writeln('  modulus of jth eigenvalue of ith model');
           writeln('  (in the order of declaration)');
           writeln('  domain: 1 <= i <= model_nb;');
           writeln('          1 <= j <= size of ith model');
           writeln('  lambdaf(i,1) = dominant eigenvalue of ith model');
         end
       else
       if ( s = 'prevf') then
         begin
           writeln('prevf(x,i)');
           writeln('  previous value of variable x');
           writeln('  i integer 0 <= i <= 100');
           writeln('  value of variable x at time t-i');
         end
       else
       if ( s = 'textf') then
         begin
           writeln('textf(x)');
           writeln('  extinction time of variable x');
           writeln('  textf(x) = t as soon as x < 1');
           writeln('  otherwise textf(x) = 0');
         end
       else
       if ( s = 'meanf') then
         begin
           writeln('meanf(x)');
           writeln('  mean of variable x along time');
         end
       else
       if ( s = 'variancef') then
         begin
           writeln('variancef(x)');
           writeln('  variance of variable x along time');
         end
       else
       if ( s = 'skewnessf') then
         begin
           writeln('skewnessf(x)');
           writeln('  skewness of variable x along time');
         end
       else
       if ( s = 'cvf') then
         begin
           writeln('cvf(x)');
           writeln('  coefficient of variation of variable x along time');
           writeln('  CV(x) = sqrt(variancef(x)/meanf(x)');
         end
       else
       if ( s = 'meanzf') then
         begin
           writeln('meanzf(x)');
           writeln('  mean of variable x along time');
           writeln('  with "zero" values excluded');
           writeln('  i. e. values < extinction_threshold (default 1)');
         end
       else
       if ( s = 'variancezf') then
         begin
           writeln('variancezf(x)');
           writeln('  mean of variable x along time');
           writeln('  with "zero" values excluded');
           writeln('  i. e. values < extinction_threshold (default 1)');
         end
       else
       if ( s = 'cvzf') then
         begin
           writeln('cvzf(x)');
           writeln('  coefficient of variation of variable x along time');
           writeln('  with "zero" values excluded');
           writeln('  i. e. values < extinction_threshold (default 1)');
           writeln('  CVZ(x) = sqrt(variancezf(x)/meanzf(x)');
         end
       else
       if ( s = 'nzf') then
         begin
           writeln('nzf(x)');
           writeln('  number of "zero" values of variable x along time');
           writeln('  i. e. number of values < extinction_threshold (default 1)');
         end
       else
       if ( s = 'nef') then
         begin
           writeln('nef(x)');
           writeln('  number of "extinctions" of variable x along time');
           writeln('  i. e. number of dates s <= t such that');
           writeln('  x(s-1) >= extinction_threshold and');
           writeln('  x(s)< extinction_threshold (default 1)');
         end
       else
       if ( s = 'nif') then
         begin
           writeln('nif(x)');
           writeln('  number of "immigrations" of variable x along time');
           writeln('  i. e. number of dates s <= t such that');
           writeln('  x(s-1) < extinction_threshold and');
           writeln('  x(s) >= extinction_threshold (default 1)');
         end
       else
       if ( s = 'extratef') then
         begin
           writeln('extratef(x)');
           writeln('  extinction rate of variable x along time');
           writeln('  = probability that x(t) < extinction_threshold (default 1)');
           writeln('  and x(t-1) >= extinction_threshold (default 1)');
         end
       else
       if ( s = 'immratef') then
         begin
           writeln('immratef(x)');
           writeln('  immigration rate of variable x along time');
           writeln('  = probability that x(t-1) < extinction_threshold (default 1)');
           writeln('  and x(t) >= extinction_threshold (default 1)');
         end;
     end;
  end;
end;

procedure interp(s : string);
var  pos : integer;
     c : char;
     s1,s2 : string;
begin
  s := tronque(minuscule(s));
  if ( s = '' ) then exit;
  pos := position(s,' ');
  if ( pos > 0 ) then
    begin
      coupe(s,pos,s1,s2);
      c := traduc(s1);
      s := tronque(s2);
    end
  else
    begin
      c := traduc(s);
      s := '';
    end;
  case c of
  'a'   : interp_param(s);
  'c'   : interp_chg_variable(s);
  'f'   : interp_fic(s);
  'h'   : interp_help(s);
  'i'   : interp_init(s);
  'j'   : interp_scatter(s);
  'm'   : interp_run_carlo(s);
  'n'   : interp_newvar(s);
  'p'   : interp_prop(s);
  'q'   : interp_quit;
  'r'   : interp_run(s);
  's'   : interp_sensibilite(s);
  't'   : interp_texte(s);
  'v'   : interp_voir(s);
  'w'   : interp_lyap(s);
  'z'   : interp_calc;
  '_'   : interp_info;
  '{'   : interp_comment(s);
  ';'   : interp_tobs(s); {ccccc}
  else;
  end;
end;

end.

